<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable=['date','description'];

    public function purchase_item()
    {
        return $this->hasMany('App\Purchase_Item','purchases_id');
    }


    public function purchase_order_item()
    {
        return $this->hasMany('App\PurchaseOrderItems', 'purchases_id');
    }


    //Cascade delete
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($purchase) {
            $items=\App\Purchase_Item::where('purchases_id','=',$purchase->id)->get();
            foreach($items as $item)
            {
                //To get current unit rate;
                $unit=\App\Unit::find($item->units_id);
                $curQty=0;

                if($unit->isunit==true)
                    $curQty=$item->qty*$unit->rate;
                else
                    $curQty=$item->qty;

                $row=\App\Row_Meterial::find($item->row_meterials_id);

                $row->qty-=$curQty;
                $row->save();
            }

            $purchase->purchase_item()->delete();
        });
    }
}
