<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable =['order_date','description','branches_id'];

    public function branch()
    {
        return $this->belongsTo('App\Branch','branches_id');
    }
    public function order_item()
    {
        return $this->hasMany('App\Order_Item','orders_id');
    }
//Cascade delete
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($order) {
            $order->order_item()->delete();
        });
    }
}
