<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_Item extends Model
{
    protected $table='order_items';
    protected $fillable=['orders_id','row_meterials_id','qty','units_id','balance','priority','assign'];

    /**
     * @return mixed
     */
    public function order()
    {
        return $this->belongsTo('App\Order','orders_id');
    }
    public function row_meterial()
    {
        return $this->belongsTo('App\Row_Meterial','row_meterials_id');
    }
    public function unit()
    {
        return $this->belongsTo('App\Unit','units_id');
    }

}
