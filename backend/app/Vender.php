<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vender extends Model
{
    protected $fillable=['name','address','place','phone','mobile','contact_person'];

    public function row_meterial()
    {
        return $this->hasMany('App\Row_Meterial');
    }
}
