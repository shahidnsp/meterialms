<?php

namespace App\Helpers;

/**
 * All user defined helper function for app.
 * User: Shahid Neermunda
 * Date: 09/06/16
 * Time: 5:57 PM
 */

class UserHelper
{

    /**
     * Get all not permitted pages
     * @param String $userPermission permission User to find
     * @param bool $isPlain get only page
     * @param bool $all get all permission including write and edit
     * @return array
     */

    public static function notPages($userPermission)
    {
        $pages = [];
        $allPages = \Config::get('app.pages');

        //Check if no permissions are set
        if(strlen($userPermission) == 0)
            return $allPages;

        $permissions = explode(',',$userPermission);

        foreach($permissions as $key =>$permission){

            $pagePermission = str_split($permission);

            if($pagePermission[0] == '0'){

                if(isset($allPages[$key])){
                    $page = $allPages[$key];
                    array_push($pages,$page);
                }
            }
        }

        return $pages;
    }

    /**
     *Get all permitted pages
     * @param String $userPermission permission User to find
     * @param bool $isPlain get only page
     * @param bool $all get all permission including write and edit
     * @return array
     */

    public static function pages($userPermission,$isPlain=false,$all=false,$isTitle=false)
    {
        $pages = [];
        $allPages = \Config::get('app.pages');

        $permissions = explode(',',$userPermission);

        foreach($permissions as $key =>$permission){

            $pagePermission = str_split($permission);

            if($pagePermission[0] == '1'){
                if(isset($allPages[$key])){
                    $page = $allPages[$key];
                    if($all){
                        $page['write'] = ($pagePermission[1] == '1')?'true':'false';
                        $page['edit'] = ($pagePermission[2] == '1')?'true':'false';
                    }
                    if($isTitle)
                        $pages[$page['name']] = $page;
                    else
                        array_push($pages,$page);
                }
            }
        }

        if($isPlain)
            array_walk($pages,function(&$page){
                $page = $page['name'];
            });
        return $pages;
    }

    /***
     * @param $jsonPermission
     * @return string with permission
     */

    public static function makePermission($jsonPermission)
    {
        $permisisons =[];

//        $pages = json_decode($jsonPermission);
        $pages = $jsonPermission;
        $allPages = \Config::get('app.pages');

        foreach($allPages as $page){

            if(($key = self::findPage($pages,$page['name'])) !== false){
                $permission = '1';
//                $permission .= $pages[$key]->write === 'true'?'1':'0';
//                $permission .= $pages[$key]->edit === 'true'?'1':'0';

                $permission .= $pages[$key]['write'] === 'true'?'1':'0';
                $permission .= $pages[$key]['edit']=== 'true'?'1':'0';

                array_push($permisisons,$permission);
            }
            else
                array_push($permisisons,'000');
        }

        return implode(',',$permisisons);
    }

    public static function allPages()
    {

        return \Config::get('app.pages');
    }

    private static function findPage(array $pages, $needle)
    {
        foreach($pages as $key =>$page){
            if($page['name'] == $needle)
                return $key;
        }
        return false;
    }
}