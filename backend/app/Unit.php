<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable=['name','isunit','rate'];

    public function order_item()
    {
        return $this->hasMany('App\Order_Item','units_id');
    }

    public function sales_item()
    {
        return $this->hasMany('App\Sale_Item','units_id');
    }

    public function row_meterial()
    {
        return $this->hasMany('App\Row_Meterial','units_id');
    }

    public function purchase_item()
    {
        return $this->hasMany('App\Purchase_Item','units_id');
    }

    public function purchase_order_item()
    {
        return $this->hasMany('App\PurchaseOrderItems', 'units_id');
    }

}
