<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password','branches_id','address','isAdmin','phone','mobile','permission'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] =bcrypt($password);
    }

    public function branch()
    {
        return $this->belongsTo('App\Branch','branches_id');
    }
    public function message()
    {
        return $this->hasMany('App\Message','users_id');
    }

    public function task()
    {
        return $this->hasMany('app\task');
    }

    /**
     * Change password or current password
     * @param $oldPassword
     * @param $password
     * @return boolean true if password changed and false if old password doesn't match
     */
    public function changePassword($oldPassword, $password)
    {
        //if(Hash::check('admin',$this->attributes['password'])){
        if(Hash::check($oldPassword,$this->attributes['password'])){
            $this->attributes['password'] = bcrypt($password);
            return true;
        }
        return false;
    }

}
