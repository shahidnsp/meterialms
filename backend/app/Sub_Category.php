<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_Category extends Model
{
    protected $fillable = ['name', 'description','main_categories_id'];
    protected $table ='sub_categories';

    public function main_category()
    {
        return $this->belongsTo('App\Main_Category','main_categories_id');
    }

    public function row_meterial()
    {
        return $this->hasMany('App\Row_Meterial','sub_categories_id');
    }

}
