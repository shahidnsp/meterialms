<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storage_Location extends Model
{
   protected $table ='storage_locations';
    protected $fillable=['name','description'];

    public function row_meterial()
    {
        return $this->hasMany('App\Row_Meterial','storage_locations_id');

    }
}
