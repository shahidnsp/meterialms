<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Main_Category extends Model
{
    protected $fillable = ['name', 'description'];
    protected $table='main_categories';

    public function sub_category()
    {
        return $this->hasMany('App\Sub_Category','main_categories_id');
    }

    public function row_meterial()
    {
        return $this->hasMany('App\Row_Meterial','main_categories_id');
    }
}
