<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable=['name','address','place','phone','mobile','supervisor'];

    public function user()
    {
        return $this->hasMany('App\User','branches_id');
    }
    public function order()
    {
        return $this->hasMany('App\Order','branches_id');
    }
}
