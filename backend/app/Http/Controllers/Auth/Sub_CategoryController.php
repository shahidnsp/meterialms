<?php

namespace App\Http\Controllers\Auth;

use App\Sub_Category;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Response;
use Vaidator;
use Auth;
use App\Http\Controllers\Controller;

class Sub_CategoryController extends Controller
{

    /**
     * @param array $data
     * @return mixed
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'main_categories_id' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Sub_Category::with('main_category')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $sub_category = new Sub_Category($request->all());
        if ($sub_category->save()) {
            return $sub_category;
        }

        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $sub_category = Sub_Category::find($id);
        if ($sub_category->update($request->all())) {
            return $sub_category;
        }

        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO model cascade delete

        if (Sub_Category::destroy($id)) {
            return Response::json(['msg' => 'Sub categories deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 500);
        }
    }
}
