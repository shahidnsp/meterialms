<?php

namespace App\Http\Controllers\Auth;

use App\Purchase;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
class PurchaseController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        // Branch is not validated
        return Validator::make($data,[
            'date'  => 'required'
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPurchaseBetweenDate(Request $request)
    {
        $from=$request->get('fromDate');
        $to=$request->get('toDate');
        return Purchase::where('date','>=',$from)
            ->where('date','<=',$to)->with('purchase_item')->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        return Purchase::where('date','>=',$from)->where('date','<=',$to)->with('purchase_item')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $purchase = new Purchase($request->all());
        if($purchase->save()){
            return $purchase;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $purchase = Purchase::find($id);

        if($purchase->update($request->all())){
            return $purchase;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Purchase model for cascade delete
        if(Purchase::destroy($id))
            return Response::json(array('msg'=>'Purchase deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
