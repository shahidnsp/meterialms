<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\Http\Controllers\Controller;
use App\PurchaseOrder;
use App\Http\Requests;
use Response;
use Validator;

class PurchaseOrderController extends Controller
{

    /**
     * Validate the own requests
     * @return validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'order_date' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PurchaseOrder::with('order_item.row_meterial','branch')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }
        $purchase_order = new PurchaseOrder($request->all());
        if ($purchase_order->save()) {
            return $purchase_order;
        } else {
            return Response::json(['error' => 'Server error'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PurchaseOrder::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }
        $purchase_order = PurchaseOrder::find($id);

        if ($purchase_order->update($request->all())) {
            return $purchase_order;
        }
        return Response::json(['error' => 'Server is Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (PurchaseOrder::destroy($id)) {
            return Response::json(['msg' => 'Purchase order table record deleted']);
        } else {
            return Response::json(['error' => 'Records not found']);
        }

    }
}
