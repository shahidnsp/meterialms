<?php

namespace App\Http\Controllers\Auth;

use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Response;
use Validator;

use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    /**
     * validating required field
     * @param array $data
     * @return mixed
     */
    public function validator(array $data)
    {
        return Validator::make($data,[
            'message'=>'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from=new Carbon('first day of last month');
        $to=new Carbon('last day of last month');

        Message::where('date','>=',$from)->where('date','<=',$to)->delete();

        return Message::with('user')->orderBy('date', 'DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $message= new Message($request->all());
        $message->users_id=Auth::id();
        $date=Carbon::now();
        $message->date=$date->toDateTimeString();
        if($message->save()){
            return $message;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $message = Message::find($id);

        if($message->update($request->all())){
            return $message;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(Message::destroy($id))
            return Response::json(array('msg'=>'Message deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
