<?php

namespace App\Http\Controllers\Auth;

use App\Branch;
use App\Order_Item;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{

    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {

        // Branch is not validated
        return Validator::make($data,[
            'name'  => 'required',
            'address'   => 'required|max:255',
            'place'     => 'required',
            'supervisor'     => 'required|max:255'
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Branch::with('order')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getBranchCount(){
        return Branch::count();
    }

    public function getBranch(){
        return Branch::select('id','name')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $branch = new Branch($request->all());
        if($branch->save()){
            return $branch;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Branch::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $branch = Branch::find($id);

        if($branch->update($request->all())){
            return $branch;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Branch model for cascade delete
        if(Branch::destroy($id))
            return Response::json(array('msg'=>'Branch deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
