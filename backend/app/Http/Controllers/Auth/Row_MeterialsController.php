<?php

namespace App\Http\Controllers\Auth;

use App\Order_Item;
use App\Row_Meterial;
use Faker\Provider\File;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Mockery\CountValidator\Exception;
use Response;
use Validator;
use Auth;
use Input;
use App\Http\Controllers\Controller;

class Row_MeterialsController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'qty' => 'required',
            'units_id' => 'required',
            'main_categories_id' => 'required',
            'venders_id' => 'required',
            'storage_locations_id' => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Row_Meterial::with('unit','main_category','sub_category','vendor','storage_location')->orderBy('name', 'asc')->get();
    }

    public function warningReminder(){
        $list=[];
        $materials= Row_Meterial::all();
        foreach($materials as $material){
           $qty=$material->qty;
           $warning=$material->warning_qty;
           if($qty<=$warning)
               array_push($list,$material);
        }
        return $list;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getRowMaterialCount(){
        return Row_Meterial::count();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        if($request->id==null){
            $row_material = new Row_Meterial($request->all());

            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $row_material->photo = $this->uploadImage($files);
                }
                //}
            }

            if ($row_material->save()) {
                return $row_material;
            }
        }else{
            $row_material = Row_Meterial::find($request->id);


            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $row_material->photo = $this->uploadImage($files);
                }
            }

            $row_material->name=$request->name;
            $row_material->description=$request->description;
            $row_material->color=$request->color;
            $row_material->size=$request->size;
            $row_material->qty=$request->qty;
            $row_material->warning_qty=$request->warning_qty;
            $row_material->units_id=$request->units_id;
            $row_material->main_categories_id=$request->main_categories_id;
            $row_material->sub_categories_id=$request->sub_categories_id;
            $row_material->venders_id=$request->venders_id;
            $row_material->storage_locations_id=$request->storage_locations_id;

            if ($row_material->save()) {
                return $row_material;
            }
        }

        return Response::json(['error' => 'Server down'], 500);

    }

    /**
     * Upload file into server
     * @param $file
     * @return string
     */
    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = rand(11111,99999).'.'.$extension;
            $imageRealPath 	= 	$file->getRealPath();
            $img = Image::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval('500'), null, function($constraint) {
                $constraint->aspectRatio();
            });
            $storedFileName='/img/upload/'. $fileName;
            //$storedFileName=$fileName;
           /* $file->move(
                public_path().'/img/upload/', $fileName
            );*/

            $img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;
            $image = Image::make($file->getRealPath())->resize(200, 200)->save($path);*/

            //Storage::disk('local')->put($fileName,  file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item= Row_Meterial::with('unit','main_category','sub_category','vendor','storage_location')->find($id);

        //$item->photo=Storage::disk('local')->get($item->photo);
        return $item;
    }

    public function stockReport(){
        $materials=Row_Meterial::all();

        foreach($materials as $material){
            $order=Order_Item::where('status',0)->where('row_meterials_id',$material->id)->sum('qty');
            $material['order']=$order;
        }

        foreach($materials as $material){
            $pending=Order_Item::where('status',0)->where('row_meterials_id',$material->id)->sum('balance');
            $material['pending']=$pending;
        }

        return $materials;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->erros(), 400);
        }

        $row_material = Row_Meterial::find($id);


        if ($request->hasFile('photo')) {
            $files = $request->file('photo');
            $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
            $validator = Validator::make(array('photo'=> $files), $rules);
            if($validator->passes()){
                $row_material->photo = $this->uploadImage($files);
            }
        }

        $row_material->name=$request->name;
        $row_material->description=$request->description;
        $row_material->color=$request->color;
        $row_material->size=$request->size;
        $row_material->qty=$request->qty;
        $row_material->warning_qty=$request->warning_qty;
        $row_material->units_id=$request->units_id;
        $row_material->main_categories_id=$request->main_categories_id;
        $row_material->sub_categories_id=$request->sub_categories_id;
        $row_material->venders_id=$request->venders_id;
        $row_material->storage_locations_id=$request->storage_locations_id;

        if ($row_material->save()) {
            return $row_material;
        }
        return Response::json(['error' => 'server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row=Row_Meterial::find($id);
        /*try{
            if($row->photo!=null)
                unlink(public_path($row->photo));
        }catch (Exception $e){}*/
        if($row->photo!=''){
            if (file_exists(public_path($row->photo))) {
                unlink(public_path($row->photo));
            }
        }

        if (Row_Meterial::destroy($id)) {
            return Response::json(['msg' => 'Row Material deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }



}
