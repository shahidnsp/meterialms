<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use Auth;

use App\User;

class UserController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'username'  =>'required|alpha_dash',
            'name'      =>'required|alpha',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //

        if(Auth::id() == 1)
            return User::where('id','!=',\Auth::id())->with('branch')->get();
        else
            return Response::json(['error'=>'Forbidden'],403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $user = new User($request->all());

        $user->isAdmin = 'u';
        $user->password = 'admin';
        $user->permission='000,000,000,000,000,000,000,000,000,000,000';

        if($user->save()){
            return $user;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    public function getUserCount(){
        return User::count();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::with('branch')->findOrFail($id);

        return $user;
    }

    public function getBranchUsers($id)
    {
        return User::where('branches_id','=',$id)->count();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $user = User::find($id);
        $user->fill($request->all());

        if($user->save()){
            return $user;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO cascade

        if(User::destroy($id))
            return Response::json(array('msg'=>'User deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
