<?php

namespace App\Http\Controllers\Auth;

use App\Order_Item;
use App\Unit;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
class Order_ItemController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        // Order is not validated
        return Validator::make($data,[
            'orders_id'  => 'required',
            'row_meterials_id'   => 'required',
            'qty'     => 'required',
            'units_id'     => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order_Item::with('order','row_meterial','unit')->get();
    }

    public function pending_order()
    {
        return Order_Item::where('status','=','0')->with('order','row_meterial','unit')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $order = new Order_Item($request->all());
        if($order->save()){
            return $order;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Order_Item::where('orders_id','=',$id)->with('order','row_meterial','unit')->get();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function newcount()
    {
        return Order_Item::where('status','=','0')->count();
    }

    public function pendingcount()
    {
        return Order_Item::where('status','=','1')->count();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $order = Order_Item::find($id);


        if($request->assign===0 || $request->assign==='0.00' || $request->assign===0.00) {
            $order->assign=0;
            $order->balance=0;
            $order->status='0';
        }else{
            if($request->balance===0 || $request->balance==='0.00' || $request->balance==='0' || $request->balance===0.00)
                $order->status='1';
            $order->assign=$request->assign;
            $order->balance=$request->balance;
        }


        //To get current unit rate;
        $unit=Unit::find($request->units_id);
        $previousQty=0;
        if($unit->isunit==true)
            $previousQty=$request->previous*$unit->rate;
        else
            $previousQty=$request->previous;

        $row=\App\Row_Meterial::find($request->row_meterials_id);

        $row->qty+=$previousQty;
        $row->save();


        /**
         * Update Stock
         */
        $curQty=0;

        if($unit->isunit==true)
            $curQty=$order->assign*$unit->rate;
        else
            $curQty=$order->assign;


        /*
         * Update Stock
         */
        $row=\App\Row_Meterial::find($request->row_meterials_id);

        $row->qty-=$curQty;
        $row->save();

        if($order->update()){
            return $order;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Order_Item::where('orders_id','=',$id)->delete())
            return Response::json(array('msg'=>'Order item deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
