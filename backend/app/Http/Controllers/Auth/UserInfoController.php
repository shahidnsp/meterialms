<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Response;
use Auth;

use App\Http\Controllers\Controller;

use App\Helpers\UserHelper;
use App\User;

class UserInfoController extends Controller
{
    const ALL_PERMISSION = true;
    const ONLY_PAGE = true;

    public function UserInfoController()
    {
        $user=User::with('branch')->find(Auth::id());
        $userInfo['id']        = $user->id;
        $userInfo['name'] = $user->name;
        $userInfo['menu']      = UserHelper::pages($user->permission);
        $userInfo['permissions'] = UserHelper::pages($user->permission,false,true,true);
        $userInfo['isAdmin']   = $user->isAdmin;
        $userInfo['branch']=$user->branch->name;
        return $userInfo;
    }

    public function updateUserInfo($id,Request $request)
    {
        $userdata = $request->only('id','name','address','phone','mobile');

        $user = User::findOrFail($id);
        $user->update($userdata);

        return $this->getUserInfo($id);
    }

    protected function getUserInfo($id)
    {
        $user = User::findOrFail($id);
        $userData['id'] = $user->id;
        $userData['name'] = $user->name;
        $userData['address']=$user->address;
        $userData['phone']=$user->phone;
        $userData['mobile']=$user->mobile;
        $userData['branch']=$user->branch;
        return $userData;
    }

    public function resetUserPassword(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);
                //TODO add settings for reset password
                $user->password = 'admin';
                if($user->save())
                    return $user;
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }

    }

    public function setUserPassword(Request $request)
    {
        $data = $request->only('oldPassword','password','username');
        $validator = Validator::make($data,[
            'oldPassword'=>'required',
            'password'=>'required',
            'username'=>'required'
        ]);


        if($validator->passes()){
            try{

                $user = User::findOrFail(Auth::id());
            }
            catch(ModelNotFoundException $e)
            {
                return Response::json(['error'=>'Requested user not found'],404);
            }


            if($user->changePassword($data['oldPassword'],$data['password'])){
                $user->username=$data['username'];
                if($user->save())
                    return $user;
            }
            else
                return Response::json(['error'=>'Old password does not match'],400);
        }
        else
            return Response::json(['errors'=>$validator->errors()],400);
    }

    public function getUserPermission(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);

                $permission =
                    UserHelper::pages($user->permission,!self::ONLY_PAGE,self::ALL_PERMISSION);

                return Response::json($permission);
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }
    }

    public function setUserPermission(Request $request)
    {
        if($request->only('id','permission'))
        {
            $permission= UserHelper::makePermission($request->input('permission'));

            $user = User::findOrFail($request->input('id'));
            $user->permission = $permission;
            if($user->save())
                return $user;
            return Response::json(['error'=>'Server is down'],500);
        }

        return Response::json(['error'=>'bad request'],400);
    }

    public function getAllPages($id)
    {
        return $userPages = UserHelper::notPages(User::find($id)->permission);
    }
}
