<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\PurchaseOrderItems;
use Response;
use Validator;

class PurchaseOrderItemController extends Controller
{

    /**
     * Validate the own request
     */
    protected function validator(array $data)
    {
        // Order is not validated
        return Validator::make($data,[
            'purchases_id'  => 'required',
            'row_meterials_id'   => 'required',
            'qty'     => 'required',
            'units_id'     => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PurchaseOrderItems::with('purchase')->with('row_meterial')->with('unit')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $purchase_order_item = new PurchaseOrderItems($request->all());

        if ($purchase_order_item->save()) {
            return $purchase_order_item;
        } else {
            return Response::json(['error' => 'server Down.'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PurchaseOrderItems::with('order')->with('row_meterial')->with('unit')->where('purchases_id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $purchase_order_item = PurchaseOrderItems::find($id);

        if ($purchase_order_item->update($request->all())) {
            return $purchase_order_item;
        } else {
            return Response::json(['error' => 'Server is Down'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(PurchaseOrderItems::where('purchases_id','=',$id)->delete())
            return Response::json(array('msg'=>'Order item deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
