<?php

namespace App\Http\Controllers\Auth;

use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        // Order is not validated
        return Validator::make($data,[
            'order_date'  => 'required',
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */

    public function getOrderBetweenDate(Request $request)
    {
        $from=$request->get('fromDate');
        $to=$request->get('toDate');

        $user=User::find(Auth::id());
        if($user->isAdmin==='A')
            return Order::where('order_date','>=',$from)
                ->where('order_date','<=',$to)->with('order_item.row_meterial','branch')->get();
        else
            return Order::where('order_date','>=',$from)
                ->where('order_date','<=',$to)->where('branches_id','=',$user->branches_id)->with('order_item.row_meterial','branch')->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        $user=User::find(Auth::id());
        if($user->isAdmin==='A')
            return Order::where('order_date','>=',$from)->where('order_date','<=',$to)->with('order_item.row_meterial','branch')->get();
        else
            return Order::with('order_item.row_meterial','branch')->where('branches_id','=',$user->branches_id)->where('order_date','>=',$from)->where('order_date','<=',$to)->get();
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $order = new Order($request->all());

        $user=User::find(Auth::id());
        $order->branches_id=$user->branches_id;

        if($order->save()){
            return $order;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $order = Order::find($id);

        if($order->update($request->all())){
            return $order;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Branch model for cascade delete
        if(Order::destroy($id))
            return Response::json(array('msg'=>'Order deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
