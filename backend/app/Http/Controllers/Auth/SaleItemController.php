<?php

namespace App\Http\Controllers\Auth;

use App\Sale_Item;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class SaleItemController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        // Order is not validated
        return Validator::make($data,[
            'sales_id'  => 'required',
            'row_meterials_id'   => 'required',
            'qty'     => 'required',
            'units_id'     => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Sale_Item::with('sale','row_meterial','unit')->get();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $sale = new Sale_Item($request->all());
        if($sale->save()){
            return $sale;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Sale_Item::where('sales_id','=',$id)->with('sale','row_meterial','unit')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $sale = Sale_Item::find($id);

        if($sale->update()){
            return $sale;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Sale_Item::where('sales_id','=',$id)->get();

        foreach($items as $item)
        {
            //To get current unit rate;
            $unit=\App\Unit::find($item->units_id);
            $curQty=0;

            if($unit->isunit==true)
                $curQty=$item->qty*$unit->rate;
            else
                $curQty=$item->qty;

            $row=\App\Row_Meterial::find($item->row_meterials_id);

            $row->qty+=$curQty;
            $row->save();
        }

        if(Sale_Item::where('sales_id','=',$id)->delete())
            return Response::json(array('msg'=>'Sales item deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
