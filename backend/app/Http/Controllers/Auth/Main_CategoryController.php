<?php

namespace App\Http\Controllers\Auth;

use App\Main_Category;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class Main_CategoryController extends Controller
{

    /**
     * @param array $data
     * @return mixed
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Main_Category::with('row_meterial')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $main_category = new Main_Category($request->all());
        if ($main_category->save()) {
            return $main_category;
        }

        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $main_category = Main_Category::find($id);

        if ($main_category->update($request->all())) {
            return $main_category;
        }

        return Response::json(['error' => 'server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO model for cascade delete

        if (Main_Category::destroy($id)) {
            return Response::json(['msg' => 'Main_category deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
