<?php

namespace App\Http\Controllers\Auth;
use App\Purchase_Item;
use App\Unit;
use Illuminate\Http\Request as Request;
use Response;
use Validator;
use Auth;
use App\Http\Requests;

use App\Http\Controllers\Controller;

class Purchase_ItemController extends Controller
{

    public function validator(array $data)
    {
        return Validator::make($data,[
            'qty'=>'required',
            'purchases_id'=>'required',
            'row_meterials_id'=>'required',
            'units_id'=>'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Purchase_Item::with('purchase','row_meterial','unit')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $item= new Purchase_Item($request->all());
        if($item->save()){
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Purchase_Item::where('purchases_id','=',$id)->with('purchase','row_meterial','unit')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $item = Purchase_Item::find($id);

        if($item->update($request->all())){
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Purchase_Item::where('purchases_id','=',$id)->get();

        foreach($items as $item)
        {
            //To get current unit rate;
            $unit=Unit::find($item->units_id);
            $curQty=0;

            if($unit->isunit==true)
                $curQty=$item->qty*$unit->rate;
            else
                $curQty=$item->qty;

            $row=\App\Row_Meterial::find($item->row_meterials_id);

            $row->qty-=$curQty;
            $row->save();
        }

        if(Purchase_Item::where('purchases_id','=',$id)->delete())
            return Response::json(array('msg'=>'Purchase item deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
