<?php

namespace App\Http\Controllers\Auth;

use App\Unit;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use Response;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

class UnitController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        // Branch is not validated
        return Validator::make($data,[
            'name'  => 'required'
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Unit::with('purchase_item')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $unit = new Unit($request->all());
        if($unit->save()){
            return $unit;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Unit::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $unit = Unit::find($id);

        if($unit->update($request->all())){
            return $unit;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Unit model for cascade delete
        if(Unit::destroy($id))
            return Response::json(array('msg'=>'Unit deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
