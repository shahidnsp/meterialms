<?php

namespace App\Http\Controllers\Auth;

use App\Vender;
use Illuminate\Http\Request as Request;
use Response;
use Validator;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class VenderController extends Controller
{
    /**
     * validating required field
     * @param array $data
     * @return mixed
     */

    public function validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'address' =>'required',
            'phone'=>'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Vender::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $vender= new Vender($request->all());
        if($vender->save()){
            return $vender;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $vender = Vender::find($id);

        if($vender->update($request->all())){
            return $vender;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Vender::destroy($id))
            return Response::json(array('msg'=>'Vender deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
