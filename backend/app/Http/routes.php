<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function () {
    //return view('welcome');
    if(\Illuminate\Support\Facades\Auth::check()) {
        return redirect('/dashboard');
    } else {
        return view('auth.login');
    }
    
}]);

Route::group(['prefix' => 'user'], function () {

    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
});

Route::group(['middleware'=>'auth'],function() {
// Loggedin page
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Auth\DashboardController@index']);
});


//TODO add middleware to authenticate
//Route::group(['middleware'=>'auth','prefix'=>'api'],function(){
Route::group(['prefix' => 'api'], function () {

    //Shahid.................
    Route::get('userinfo',['as'=>'userinfo','uses'=>'Auth\UserInfoController@UserInfoController']);

    Route::put('userinfo/{id}',['as'=>'userinfo','uses'=>'Auth\UserInfoController@updateUserInfo']);

    Route::get('getAllPages/{id}',['as'=>'getAllPages','uses'=>'Auth\UserInfoController@getAllPages']);
    Route::post('changePassword',['as'=>'changePass','uses'=>'Auth\UserInfoController@setUserPassword']);
    Route::post('resetPassword',['as'=>'resetPass','uses'=>'Auth\UserInfoController@resetUserPassword']);
    Route::get('getPermission',['as'=>'getPermission','uses'=>'Auth\UserInfoController@getUserPermission']);
    Route::post('changePermission',['as'=>'setPermission','uses'=>'Auth\UserInfoController@setUserPermission']);

    //Rest resources
    Route::resource('branch', 'Auth\BranchController');
    Route::resource('unit', 'Auth\UnitController');
    Route::resource('location', 'Auth\Storage_LocationController');
    Route::resource('order', 'Auth\OrderController');
    Route::resource('orderitem', 'Auth\Order_ItemController');
    Route::get('pending_order', 'Auth\Order_ItemController@pending_order');
    Route::resource('purchase', 'Auth\PurchaseController');
    Route::resource('user', 'Auth\UserController');
    Route::resource('sale', 'Auth\SaleController');
    Route::resource('sale_item', 'Auth\SaleItemController');
    Route::get('getuser', 'Auth\UserController@index');
    Route::get('newcount', 'Auth\Order_ItemController@newcount');
    Route::get('pendingcount', 'Auth\Order_ItemController@pendingcount');
    Route::get('rowmaterialcount', 'Auth\Row_MeterialsController@getRowMaterialCount');
    Route::get('usercount', 'Auth\UserController@getUserCount');
    Route::get('branchcount', 'Auth\BranchController@getBranchCount');
    Route::get('getbranches', 'Auth\BranchController@getBranch');
    Route::get('getbranchusercount', 'Auth\UserController@getBranchUsers');
    Route::get('stockreport', 'Auth\Row_MeterialsController@stockReport');
    Route::get('warningreport', 'Auth\Row_MeterialsController@warningReminder');
    //...............Shahid

    /*noushid*/
    Route::resource('main_categories', 'Auth\Main_CategoryController');
    Route::resource('sub_categories', 'Auth\Sub_CategoryController');
    Route::resource('task', 'Auth\TaskController');
    Route::resource('row_material', 'Auth\Row_MeterialsController');
    Route::resource('purchase_order', 'Auth\PurchaseOrderController');
    Route::resource('purchase_order_item', 'Auth\PurchaseOrderItemController');

    /*noushid*/

    // Naseeba .............

    Route::resource('purchase_item', 'Auth\Purchase_ItemController');
    Route::resource('message', 'Auth\MessageController');
    Route::resource('vender', 'Auth\VenderController');

    // Naseeba


    Route::get('getsaleswithdate', 'Auth\SaleController@getSalesBetweenDate');
    Route::get('getorderwithdate', 'Auth\OrderController@getOrderBetweenDate');
    Route::get('getpurchasewithdate', 'Auth\PurchaseController@getPurchaseBetweenDate');

});



//Load angular templates
//TODO user permission validation
Route::get('template/{name}', ['as' => 'template', function ($name) {
//    $permission = UserHelper::pages(Auth::user()->permission,false,true,true);
//    return view('app.' . $name,['permission'=>$permission]);
    return view('app.' . $name);
}]);

//TODO load temp test json files
Route::get('json/{name}', function ($name) {

    return Storage::get('json/' . $name);
});

// Catch all undefined routes and give to index.php to handle  .
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('auth.login');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');


// Using different syntax for Blade to avoid conflicts with Jade.
// You are well-advised to go without any Blade at all.
Blade::setContentTags('[[', ']]'); // For variables and all things Blade.
Blade::setEscapedContentTags('[[-', '-]]'); // For escaped data.
Route::auth();


