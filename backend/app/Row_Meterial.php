<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Row_Meterial extends Model
{
    protected $table='row_meterials';

    protected $fillable = ['name', 'description', 'color', 'size', 'photo', 'qty', 'warning_qty', 'expected_date', 'units_id', 'main_categories_id', 'sub_categories_id', 'venders_id', 'storage_locations_id'];


    public function order_item()
    {
        return $this->hasMany('App\Order_Item');
    }

    public function sales_item()
    {
        return $this->hasMany('App\Sale_Item');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'units_id');
    }

    public function main_category()
    {
        return $this->belongsTo('App\Main_Category', 'main_categories_id');
    }

    public function sub_category()
    {
        return $this->belongsTo('App\Sub_Category', 'sub_categories_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vender', 'venders_id');
    }

    public function storage_location()
    {
        return $this->belongsTo('App\Storage_Location', 'storage_locations_id');
    }


    public function purchase_item()
    {
        return $this->hasMany('App\Purchase_Item');
    }

    public function purchase_order_item()
    {
        return $this->hasMany('App\PurchaseOrderItems');
    }


}
