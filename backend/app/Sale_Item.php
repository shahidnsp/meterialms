<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale_Item extends Model
{
    protected $table='sale_items';
    protected $fillable=['sales_id','row_meterials_id','qty','units_id'];

    public function sale()
    {
        return $this->belongsTo('App\Sale','sales_id');
    }
    public function row_meterial()
    {
        return $this->belongsTo('App\Row_Meterial','row_meterials_id');
    }
    public function unit()
    {
        return $this->belongsTo('App\Unit','units_id');
    }

    public static function boot()
    {
        parent::boot();

        static::updating(function($item){

        });


        static::deleted(function($item){

        });

        static::created(function($item){
            $row=\App\Row_Meterial::find($item->row_meterials_id);

            //To get current unit rate;
            $unit=\App\Unit::find($item->units_id);
            $curQty=0;

            if($unit->isunit==true)
                $curQty=$item->qty*$unit->rate;
            else
                $curQty=$item->qty;

            $row->qty-=$curQty;
            $row->save();
        });

        static::updated(function($item){
            $row=\App\Row_Meterial::find($item->row_meterials_id);

            //To get current unit rate;
            $unit=\App\Unit::find($item->units_id);
            $curQty=0;

            if($unit->isunit==true)
                $curQty=$item->qty*$unit->rate;
            else
                $curQty=$item->qty;

            $row->qty-=$curQty;
            $row->save();
        });


    }
}
