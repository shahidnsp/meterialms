<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'description', 'inactive','fromDate','toDate', 'users_id'];

    public function user()
    {
        return $this->belongsTo('App\User','users_id');
    }
}
