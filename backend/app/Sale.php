<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable=['date','name','address','phone','mobile','description'];

    public function sales_item()
    {
        return $this->hasMany('App\Sale_Item','sales_id');
    }
}
