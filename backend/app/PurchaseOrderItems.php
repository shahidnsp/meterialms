<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderItems extends Model
{
    protected $fillable = ['qty', 'purchases_id', 'row_meterials_id', 'units_id','priority'];

    public function order()
    {
        return $this->belongsTo('App\PurchaseOrder','purchases_id');
    }
    public function row_meterial()
    {
        return $this->belongsTo('App\Row_Meterial','row_meterials_id');
    }
    public function unit()
    {
        return $this->belongsTo('App\Unit','units_id');
    }
}
