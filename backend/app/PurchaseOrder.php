<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $fillable = ['order_date', 'description'];

    public function branch()
    {
        return $this->belongsTo('App\Branch','branches_id');
    }
    public function order_item()
    {
        return $this->hasMany('App\PurchaseOrderItems','purchases_id');
    }
}
