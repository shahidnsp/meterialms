var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.sass('app.scss');
    mix.rubySass('app.scss', null, {container: 'application-css', verbose: true});

    mix.copy('resources/assets/img','public/img');
    mix.copy('resources/assets/fonts','public/fonts');
    mix.scripts(['angular.min.js','angular-route.min.js','angular-resource.min.js','ui-bootstrap-tpls.min.js','desktop-notify.js','angular-web-notification.js','angular-animate.min.js','d3.min.js','angular-charts.min.js'],'public/js/angularjs.min.js')
    mix.scripts(['template/jquery-2.0.3.min.js','template/bootstrap.js','template/modernizr-2.6.2-respond-1.1.0.min.js','template/jquery.flot.js','template/jquery.flot.resize.js','template/jquery.flot.time.js','template/jquery.flot.stack.js','ui-bootstrap-tpls.min.js'],'public/js/template.min.js')
    mix.scripts(['angularscript.js','services/*.js','controller/*.js'],'public/js/app.js');
    mix.copy('resources/assets/js/template/login.js','public/js');
    mix.copy('resources/assets/sass/login.css','public/css');
    mix.copy('resources/assets/sass/magic.min.css','public/css');
    mix.version(['css/app.css','js/angularjs.min.js','js/app.js','js/template.min.js','fonts/']);
});
