<?php

use Illuminate\Database\Seeder;

class Main_CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $main_category=factory(App\Main_Category::class,10)->create();
    }
}
