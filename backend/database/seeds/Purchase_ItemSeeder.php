<?php

use Illuminate\Database\Seeder;

class Purchase_ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item=factory(App\Purchase_Item::class,10)->create();
    }
}
