<?php

use Illuminate\Database\Seeder;

class VenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vender=factory(App\Vender::class,10)->create();
    }
}
