<?php

use Illuminate\Database\Seeder;

class SaleItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salesitem=factory(App\Sale_Item::class,10)->create();
    }
}
