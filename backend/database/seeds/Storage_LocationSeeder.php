<?php

use Illuminate\Database\Seeder;

class Storage_LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $storage=factory(App\Storage_Location::class,10)->create();
    }
}
