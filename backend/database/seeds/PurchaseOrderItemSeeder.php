<?php

use Illuminate\Database\Seeder;

class PurchaseOrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchase_order_item = factory(\App\PurchaseOrderItems::class, 10)->create();
    }
}
