<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User Seed faker
        $info = $this->command;
        Model::unguard();


    \App\User::create([
        'username'=>'admin',
        'password'=>'admin',
        'isAdmin'=>'A',
        'branches_id'=>'1',
        'name'=>'Admin',
        'address'=>'Thenga Kolla',
        'phone'=>'9565656585',
        'mobile'=>'8958545855',
        'permission'=>'111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111']);


        $info->comment('Admin user created');
        $info->error('Username : admin Password:admin');

        $info->comment('Unit creating.....................................................!!!');
        \App\Unit::create([
            'name'=>'Pies',
            'isunit'=>'0']);
        \App\Unit::create([
            'name'=>'Each',
            'isunit'=>'0']);
        \App\Unit::create([
            'name'=>'Roll',
            'isunit'=>'0']);
        \App\Unit::create([
            'name'=>'Kg',
            'isunit'=>'0']);
        \App\Unit::create([
            'name'=>'Grams',
            'isunit'=>'1',
            'rate'=>'0.001']);

        \App\Branch::create([
            'name'=>'Main Branch',
            'address'=>'Address',
            'place'=>'Place',
            'phone'=>'0000000',
            'mobile'=>'000000']);

        \App\Main_Category::create([
            'name'=>'Default',
            'description'=>'Default Category']);

        \App\Sub_Category::create([
            'name'=>'Default',
            'description'=>'Default Sub Category',
            'main_categories_id'=>'1']);

        \App\Storage_Location::create([
            'name'=>'Default',
            'description'=>'Default Location']);

        \App\Vender::create([
            'name'=>'Default',
            'address'=>'Address',
            'place'=>'Place',
            'phone'=>'0000000',
            'mobile'=>'000000']);

        $info->info('User table seeding started...');
        $this->call('User_Seeder');

        $info->info('Branches table seeding started...');
        $this->call('BranchSeeder');

        $info->info('Row Material table seeding started...');
        $this->call('Row_MeterialSeeder');

        $info->info('Order table seeding started...');
        $this->call('OrderSeeder');

        $info->info('Order_Items table seeding started...');
        $this->call('OrderItemSeeder');

        $info->info('Purchase_Items table seeding started...');
        $this->call('Purchase_ItemSeeder');

        $info->info('Storage_location table seeding started...');
        $this->call('Storage_LocationSeeder');

        $info->info('Main_Category table seeding started...');
        $this->call('Main_CategorySeeder');

        $info->info('Sub_Category table seeding started...');
        $this->call('Sub_CategorySeeder');

        $info->info('Task table seeding started...');
        $this->call('TaskSeeder');

        $info->info('Message table seeding started...');
        $this->call('MessageSeeder');

        $info->info('Vender table seeding started...');
        $this->call('VenderSeeder');

        $info->info('Purchase table seeding started...');
        $this->call('PurchaseSeeder');

        $info->info('Sale table seeding started...');
        $this->call('SaleSeeder');

        $info->info('Purchase order seeding started...');
        $this->call('PurchaseOrderSeeder');

        $info->info('Purchase order item seeding started...');
        $this->call('PurchaseOrderItemSeeder');

        $info->info('Sales Items table seeding started...');
        $this->call('SaleItemSeeder');

    }
}
