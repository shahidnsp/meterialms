<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRowMeterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('row_meterials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('color');
            $table->string('size');
            $table->string('photo');
            $table->string('qty');
            $table->string('warning_qty');
            $table->string('expeted_date');
            $table->integer('units_id');
            $table->integer('main_categories_id');
            $table->integer('sub_categories_id');
            $table->integer('venders_id');
            $table->integer('storage_locations_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('row_meterials');
    }
}
