<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'password' => bcrypt(str_random(10)),
        'isAdmin'=>'u',
        'branches_id'=>$faker->randomElement([1,2,3,4,5]),
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'permission'=>'111,111,111,111,111,111,111,111,111,111,111,111,111',
        'remember_token' => str_random(10),
    ];
});

//Branches seeder $faker

$factory->define(App\Branch::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'place' => $faker->city,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'supervisor'=>$faker->firstName
    ];
});

//Row Material $faker

$factory->define(App\Row_Meterial::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'color' => $faker->colorName,
        'size'=>$faker->randomElement(['12','58','56']),
        'photo'=>$faker->randomElement(['/img/1.png','/img/2.png','/img/3.png','/img/4.png']),
        'qty'=>'200',
        'warning_qty'=>'20',
        'units_id'=>$faker->randomElement([1,2,3,4,5]),
        'main_categories_id'=>$faker->randomElement([1,2,3,4,5]),
        'sub_categories_id'=>$faker->randomElement([1,2,3,4,5]),
        'venders_id'=>$faker->randomElement([1,2,3,4,5]),
        'storage_locations_id'=>$faker->randomElement([1,2,3,4,5])
    ];
});

//Order seeder

$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        'order_date' => $faker->date,
        'description' => $faker->sentence,
        'branches_id'=>$faker->randomElement([1,2,3,4,5])
    ];
});

//Order seeder

$factory->define(App\Order_Item::class, function (Faker\Generator $faker) {
    return [
        'orders_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'row_meterials_id' =>  $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'qty' => '10',
        'units_id'=> $faker->randomElement([1,2,3,4,5]),
        'balance'=>'0',
        'status' => $faker->randomElement(['0','1']),
        'priority'=>$faker->randomElement(['High','Medium','Low'])
    ];
});

//unit seeder

$factory->define(App\Unit::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'isunit'=>$faker->randomElement([true,false]),
        'rate'=>'0'
    ];
});

//storage location

$factory->define(App\Storage_Location::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'description'=>$faker->sentence
    ];
});

//main category

$factory->define(App\Main_Category::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'description'=>$faker->text()
    ];
});

//sub category

$factory->define(App\Sub_Category::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'description'=>$faker->text(),
        'main_categories_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//task
$factory->define(App\Task::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'description'=>$faker->sentence,
        'inactive'=>$faker->randomElement([1,0]),
        'users_id'=>$faker->randomElement([1,2,3,4,5]),
        'fromDate'=>$faker->date('Y-m-d'),
        'toDate'=>$faker->date('Y-m-d'),
    ];
});

//Massage
$factory->define(App\Message::class, function (Faker\Generator $faker) {
    return [
        'message'=>$faker->sentence(),
        'date'=>$faker->date('Y-m-d'),
        'users_id'=>$faker->randomElement([1,2,3,4,5])
    ];
});

//vender
$factory->define(App\Vender::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'address'=>$faker->address,
        'place'=>$faker->city,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'contact_person'=>$faker->name,
    ];
});

//purchase_item

$factory->define(App\Purchase_Item::class, function (Faker\Generator $faker) {
    return [
            'qty'=>'3',
            'purchases_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
            'row_meterials_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
            'units_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//purchase

$factory->define(App\Purchase::class, function (Faker\Generator $faker) {
    return [
        'date'=>$faker->date('Y-m-d'),
        'description'=>$faker->text()
    ];
});

//Sales
$factory->define(App\Sale::class, function (Faker\Generator $faker) {
    return [
        'date'=>$faker->date('Y-m-d'),
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'description'=>$faker->text()
    ];
});

//Sales Item seeder

$factory->define(App\Sale_Item::class, function (Faker\Generator $faker) {
    return [
        'sales_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'row_meterials_id' =>  $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'qty' => '10',
        'units_id'=> $faker->randomElement([1,2,3,4,5]),
    ];
});

//Purchase Order seeder

$factory->define(\App\PurchaseOrder::class, function (Faker\Generator $faker) {
    return [
        'order_date' => $faker->date,
        'description' => $faker->sentence,
        'branches_id'=>$faker->randomElement([1,2,3,4,5])
    ];
});

//Purchase order item seeder

$factory->define(\App\PurchaseOrderItems::class, function (Faker\Generator $faker) {
    return [
        'qty' => '3',
        'purchases_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'row_meterials_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'units_id'=> $faker->randomElement([1,2,3,4,5]),
        'balance'=>'0',
        'status' => $faker->randomElement(['0','1']),
        'priority'=>$faker->randomElement(['High','Medium','Low'])
    ];
});