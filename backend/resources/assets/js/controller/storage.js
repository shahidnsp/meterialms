/**
 * Created by shahi on 31/5/16.
 */
app.controller('storageController', function($scope,$http,Storage){
    $scope.storageedit = false;
    $scope.newstorage = {};
    $scope.curStorage = {};
    $scope.storages = [];

    Storage.query(function(storage){
        $scope.storages = storage;
    });

    $scope.newStorage = function () {
        $scope.storageedit = true;
        $scope.newstorage = new Storage();
        $scope.newstorage.date = new Date();
        $scope.curStorage = {};
    };
    $scope.editStorage = function (thisStorage) {
        $scope.storageedit = true;
        $scope.curStorage =  thisStorage;
        $scope.newstorage = angular.copy(thisStorage);
    };
    $scope.addStorage = function () {
        if ($scope.curStorage.id) {
            $scope.newstorage.$update(function(storage){
                angular.extend($scope.curStorage, $scope.curStorage, storage);
            });
        } else{
            $scope.newstorage.$save(function(storage){
                $scope.storages.push(storage);
            });
        }
        $scope.storageedit = false;
        $scope.newstorage = new Storage();
    };
    $scope.deleteStorage = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.storages.indexOf(item);
                $scope.storages.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelStorage = function () {
        $scope.storageedit = false;
        $scope.newstorage = new Storage();
    };
});
