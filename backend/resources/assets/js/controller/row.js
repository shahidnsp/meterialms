/**
 * Created by shahi on 01/06/16.
 */
app.controller('rowController', function($scope,$http,$modal,Row,Unit,MainCategory,Storage,Vendor,SubCategory){
    $scope.rowedit = false;
    $scope.newrow = {};
    $scope.curRow = {};
    $scope.rows = [];
    $scope.units=[];
    $scope.maincategories=[];
    $scope.storages=[];
    $scope.vendors=[];
    $scope.subcategories=[];
    $scope.selectedFile=[];

    loadRow();

    function loadRow(){
        Row.query(function(row){
            $scope.rows = row;
        });
    }


    Unit.query(function(unit){
        $scope.units = unit;
    });

    MainCategory.query(function(main){
        $scope.maincategories = main;
    });

    Storage.query(function(store){
        $scope.storages = store;
    });

    Vendor.query(function(vendor){
        $scope.vendors = vendor;
    });

    SubCategory.query(function(sub){
        $scope.subcategories = sub;
    });

    $scope.newRow = function () {
        $scope.rowedit = true;
        $scope.newrow = new Row();
        $scope.newrow.date = new Date();
        $scope.newrow.units_id=1;
        $scope.newrow.main_categories_id=1;
        $scope.newrow.sub_categories_id=1;
        $scope.newrow.venders_id=1;
        $scope.newrow.storage_locations_id=1;
        $scope.curRow = {};
    };
    $scope.editRow = function (thisRow) {
        $scope.rowedit = true;
        $scope.curRow =  thisRow;
        $scope.newrow = angular.copy(thisRow);
    };
    $scope.addRow = function () {
        if ($scope.curRow.id) {

            var fd = new FormData();
            $scope.newrow['id']=$scope.curRow.id;
            for(var key in $scope.newrow)
                fd.append(key,$scope.newrow[key]);

            var uri='api/row_material/'+$scope.curRow.id;

            $http.post('/api/row_material', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadRow();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        } else{
            var fd = new FormData();
            //fd.append('photo', $scope.myFile);

            for(var key in $scope.newrow)
                fd.append(key,$scope.newrow[key]);

            $http.post('/api/row_material', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadRow();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
        $scope.rowedit = false;
        $scope.newrow = new Row();

    };


    $scope.getTheFiles = function ($file) {
        $scope.selectedFile=$file;
    };


    $scope.deleteRow = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.rows.indexOf(item);
                $scope.rows.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelRow = function () {
        $scope.rowedit = false;
        $scope.newrow = new Row();
    };

   $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproduct',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });
    };
    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});


app.controller('viewproductController', function ($scope, $modalInstance,$modal, item) {

    $scope.product = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
