/**
 * Created by shahi on 04/06/16.
 */
app.controller('purchaseController', function($scope,$http,$modal,$filter,Purchase,Row,Unit,PurchaseItem){
    $scope.purchaseedit = false;
    $scope.newpurchase = {};
    $scope.curPurchase = {};
    $scope.purchases = [];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.rows = [];
    $scope.currow = {};
    $scope.units = [];
    $scope.curunit = {};
    $scope.editingData=[];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    Purchase.query(function(purchase){
        $scope.purchases = purchase;
    });
    Row.query(function(row){
        $scope.rows = row;
    });
    Unit.query(function(unit){
        $scope.units = unit;
    });

    $scope.searchPurchaseDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        /*$http.get('/api/getpurchasewithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.purchases=response.data;
            });*/
        Purchase.query({fromDate:from,toDate:to},function(purchase){
            $scope.purchases = purchase;
        });
    };
    //$scope.newitems=new PurchaseItem();

    $scope.newPurchase = function () {
        $scope.purchaseedit = true;
        $scope.newpurchase = new Purchase();
        $scope.newpurchase.date = new Date();
        $scope.curPurchase = {};
        $scope.newitems = [];
    };
    $scope.editPurchase = function (thisPurchase) {
        $scope.purchaseedit = true;
        $scope.curPurchase =  thisPurchase;
        $scope.newpurchase = angular.copy(thisPurchase);

        $http.get('/api/purchase_item/'+thisPurchase.id).
            success(function(data,status,headers,config){
                //$scope.newitems=data;
                $scope.newitems=[];
                for(var i=0;i<data.length;i++)
                {
                    $scope.updateitem={
                        id:data[i].id,
                        qty:data[i].qty,
                        units_id:data[i].units_id,
                        unit:data[i].unit.name,
                        row_meterials_id:data[i].row_meterials_id,
                        name:data[i].row_meterial.name,
                        priority:data[i].priority
                    };

                    $scope.newitems.push($scope.updateitem);

                    $scope.editingData[data[i].id] = false;
                }

            }).error(function(data,status,headers,config){
                console.log(data);
            });

    };
    $scope.addPurchase = function () {
        if($scope.newpurchase.date.toISOString)
            $scope.newpurchase.date = $scope.newpurchase.date.toISOString();
        if($scope.newitems.length!=0) {
            if ($scope.curPurchase.id) {

                deletepurchaseitems($scope.curPurchase.id);

                $scope.newpurchase.$update(function (purchase) {
                    angular.extend($scope.curPurchase, $scope.curPurchase, purchase);
                    savepurchaseitems(purchase.id);
                });
            } else {
                $scope.newpurchase.$save(function (purchase) {
                    $scope.purchases.push(purchase);
                    savepurchaseitems(purchase.id);
                });
            }
            $scope.purchaseedit = false;
            $scope.newpurchase = new Purchase();
        }else{
            alert('No Items in List');
        }
    };

    function savepurchaseitems(id){
        for(var i=0;i<$scope.newitems.length;i++)
        {
            $http.post('/api/purchase_item',{purchases_id:id,row_meterials_id:$scope.newitems[i].row_meterials_id,qty:$scope.newitems[i].qty,units_id:$scope.newitems[i].units_id}).
                success(function(data,status,headers,config){
                    //console.log(data);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
    }

    function deletepurchaseitems(id)
    {
        $http.delete('/api/purchase_item/' + id)
            .success(function (data, status, headers) {
                //$scope.ServerResponse = data;
            })
            .error(function(data,status,headers,config){
                console.log(data);
            });
    }


    $scope.deletePurchase = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            deletepurchaseitems(item.id);
            item.$delete(function(){
                var curIndex = $scope.purchases.indexOf(item);
                $scope.purchases.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelPurchase = function () {
        $scope.purchaseedit = false;
        $scope.newpurchase = new Purchase();
    };

    $scope.addItem = function () {
        var row=$scope.newitems.length;
        //for(var i=0;i<row;i++) {
        //  if($scope.editingData[item.id] !=true) {
        $scope.inserted = {
            id: row,
            qty: '1'
        };
        $scope.newitems.push($scope.inserted);
        $scope.editingData[row] = true;
        // }
        // }
    };

    $scope.updateItem=function(item){
        if(item.row_meterials_id!=null) {
            if(item.units_id==null)
            {
                alert('Unit not selected');
                return;
            }
            includeunit(item);
        }
    }

    function includeunit(item){
        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                $scope.curunit=data;

                $http.get('/api/row_material/'+item.row_meterials_id).
                    success(function(data,status,headers,config){
                        $scope.currow=data;

                        updateRow(item);

                        //console.log(data);
                    }).error(function(data,status,headers,config){
                        console.log(data);
                    });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            qty:item.qty,
            units_id:item.units_id,
            unit:$scope.curunit.name,
            row_meterials_id:item.row_meterials_id,
            name:$scope.currow.name,
            priority:item.priority
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        //$scope.newitems[item.id].reload();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    }

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
            var curIndex = $scope.newitems.indexOf(item);
            $scope.newitems.splice(curIndex, 1);
        }
    };

    $scope.viewItem = function (item,size) {

        var thisItem={};
        $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewPurchaseItem=function(purchase){

        $http.get('/api/purchase_item/'+purchase.id).
            success(function(data,status,headers,config){
                console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewpurchaseitems',
                    controller:'viewpurchaseitemController',
                    /*size: size,*/
                    resolve: {
                        purchase: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }
});

app.controller('viewpurchaseitemController', function ($scope, $modalInstance, purchase) {

    $scope.items = angular.copy(purchase);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
});





