/**
 * Created by shahi on 31/5/16.
 */
app.controller('subController', function($scope,$http,SubCategory,MainCategory){
    $scope.subedit = false;
    $scope.newsub = {};
    $scope.curSub = {};
    $scope.subs = [];
    $scope.category=[];

    loadData();
    function loadData() {
        SubCategory.query(function (sub) {
            $scope.subs = sub;
        });
    }

    MainCategory.query(function(cate){
        $scope.category = cate;
    });

    $scope.newSub = function () {
        $scope.subedit = true;
        $scope.newsub = new SubCategory();
        $scope.newsub.date = new Date();
        $scope.curSub = {};
    };
    $scope.editSub = function (thisSub) {
        $scope.subedit = true;
        $scope.curSub =  thisSub;
        $scope.newsub = angular.copy(thisSub);
    };
    $scope.addSub = function () {
        if ($scope.curSub.id) {
            $scope.newsub.$update(function(sub){
                angular.extend($scope.curSub, $scope.curSub, sub);
            });
        } else{
            $scope.newsub.$save(function(sub){
                $scope.subs.push(sub);
            });
        }
        $scope.subedit = false;
        $scope.newsub = new SubCategory();
        loadData();
    };
    $scope.deleteSub = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.subs.indexOf(item);
                $scope.subs.splice(curIndex, 1);
            });
        }
        loadData();
    };
    $scope.cancelSub = function () {
        $scope.subedit = false;
        $scope.newsub = new SubCategory();
    };
});
