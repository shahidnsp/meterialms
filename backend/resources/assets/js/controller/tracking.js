/**
 * Created by shahi on 04/06/16.
 */
app.controller('trackingController', function($scope,$http,$modal,$filter,Branch,Row){

    $scope.branches=[];
    $scope.rows=[];
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.branches_id=0;
    $scope.row_meterials_id=0;
    $scope.curorder=[];
    $scope.curorder_id='0';
    $scope.allorders=[];
    $scope.orders=[];
    $scope.branch_name='';
    $scope.totalAssign=0;
    $scope.totalBalance=0;
    $scope.totalQty=0;

    Branch.query(function(branch){
        $scope.branches=branch;
    });

    Row.query(function(row){
        $scope.rows=row;
    });

    $scope.search=function(from,to,row_meterials_id,branches_id){

        //var fromDate=new Date(from);
        $http.get('/api/getorderwithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.curorder=response.data;

                $scope.allorders=[];
                for(var i=0;i<$scope.curorder.length;i++) {
                   /* $scope.branch_name=$scope.curorder[i].branch.name;
                    $scope.curorder_id=$scope.curorder[i].id;*/
                    $http.get('/api/orderitem/' + $scope.curorder[i].id)
                        .then(function (response) {
                            //console.log(response.data);
                            var data=response.data;
                            /*for(var k=0;k<data.length;k++){
                                if(data[k].orders_id===$scope.curorder_id){
                                    data[k].branch=$scope.branch_name;
                                }
                            }*/

                            $scope.allorders=$scope.allorders.concat(data);
                            //$scope.orders=$scope.allorders;
                             /* angular.forEach( $scope.allorders, function(value1, j) {
                                $scope.allorders[j].branch = $scope.branch_name;
                             });*/
                        });
                }



               // console.log($scope.allorders);
                //console.log(allorders);

        });

        if(row_meterials_id =='0' && branches_id =='0') {
            $scope.orders = $scope.allorders;
            getSum($scope.orders);
        }
        else if(row_meterials_id !='0' && branches_id =='0')
        {
            $scope.orders=[];
            for(var i=0;i<$scope.allorders.length;i++) {
                if($scope.allorders[i].row_meterials_id==row_meterials_id) {
                    $scope.orders = $scope.orders.concat($scope.allorders[i]);
                    getSum($scope.orders);
                }
            }
        }
        else if(row_meterials_id =='0' && branches_id !='0')
        {
            $scope.orders=[];
            for(var i=0;i<$scope.allorders.length;i++) {
                if($scope.allorders[i].order.branches_id==branches_id) {
                    $scope.orders = $scope.orders.concat($scope.allorders[i]);
                    getSum($scope.orders);
                }
            }
        }
        else if(row_meterials_id !='0' && branches_id !='0')
        {
            $scope.orders=[];
            for(var i=0;i<$scope.allorders.length;i++) {
                if($scope.allorders[i].order.branches_id==branches_id && $scope.allorders[i].row_meterials_id==row_meterials_id) {
                    $scope.orders = $scope.orders.concat($scope.allorders[i]);
                    getSum($scope.orders);
                }
            }
        }

        for(var i=0;i<$scope.curorder.length;i++){
            for(var k=0;k<$scope.orders.length;k++){
                if($scope.orders[k].orders_id==$scope.curorder[i].id){
                    $scope.orders[k].branch=$scope.curorder[i].branch.name;
                }
            }
        }


    }

    function getSum(order){
        var assign= 0,qty= 0,balance=0;
        for(var i=0;i<order.length;i++){
            assign+=parseFloat(order[i].assign);
            qty+=parseFloat(order[i].qty);
            balance+=parseFloat(order[i].balance);
        }

        $scope.totalAssign=assign;
        $scope.totalQty=qty;
        $scope.totalBalance=balance;
    }

    $scope.viewItem = function (item,size) {

        var thisItem={};
        $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

});