/**
 * Created by shahi on 31/5/16.
 */
app.controller('unitController', function($scope,$http,Unit){
    $scope.unitedit = false;
    $scope.newunit = {};
    $scope.curUnit = {};
    $scope.units = [];

    Unit.query(function(unit){
        $scope.units = unit;
    });

    $scope.newUnit = function () {
        $scope.unitedit = true;
        $scope.newunit = new Unit();
        $scope.newunit.date = new Date();
        $scope.curUnit = {};
    };
    $scope.editUnit = function (thisUnit) {
        $scope.unitedit = true;
        $scope.curUnit =  thisUnit;
        $scope.newunit = angular.copy(thisUnit);
    };
    $scope.addUnit = function () {
        if ($scope.curUnit.id) {
            $scope.newunit.$update(function(unit){
                angular.extend($scope.curUnit, $scope.curUnit, unit);
            });
        } else{
            $scope.newunit.$save(function(unit){
                $scope.units.push(unit);
            });
        }
        $scope.unitedit = false;
        $scope.newunit = new Unit();
    };
    $scope.deleteUnit = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.units.indexOf(item);
                $scope.units.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelUnit = function () {
        $scope.unitedit = false;
        $scope.newunit = new Unit();
    };
});
