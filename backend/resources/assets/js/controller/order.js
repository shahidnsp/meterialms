/**
 * Created by shahi on 04/06/16.
 */
app.controller('orderController', function($scope,$http,$modal,$filter,Order,Row,Unit,OrderItem){
    $scope.orderedit = false;
    $scope.neworder = {};
    $scope.curOrder = {};
    $scope.orders = [];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.rows = [];
    $scope.currow = {};
    $scope.units = [];
    $scope.curunit = {};
    $scope.editingData=[];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    Order.query(function(order){
        $scope.orders = order;
    });
    Row.query(function(row){
        $scope.rows = row;
    });
    Unit.query(function(unit){
        $scope.units = unit;
    });

    $scope.searchOrderDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Order.query({fromDate:from,toDate:to},function(order){
            $scope.orders = order;
        });

        /*$http.get('/api/getorderwithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.orders=response.data;
            });*/
    };

    //$scope.newitems=new OrderItem();

    $scope.newOrder = function () {
        $scope.orderedit = true;
        $scope.neworder = new Order();
        $scope.neworder.order_date = new Date();
        $scope.curOrder = {};
        $scope.newitems = [];
    };

    $scope.editOrder = function (thisOrder) {
        $scope.orderedit = true;
        $scope.curOrder =  thisOrder;
        $scope.neworder = angular.copy(thisOrder);

        $http.get('/api/orderitem/'+thisOrder.id).
            success(function(data,status,headers,config){
                $scope.newitems=[];
                for(var i=0;i<data.length;i++)
                {
                    $scope.updateitem={
                        id:data[i].id,
                        qty:data[i].qty,
                        units_id:data[i].units_id,
                        unit:data[i].unit.name,
                        row_meterials_id:data[i].row_meterials_id,
                        name:data[i].row_meterial.name,
                        priority:data[i].priority
                    };

                    $scope.newitems.push($scope.updateitem);

                    $scope.editingData[data[i].id] = false;
                }

            }).error(function(data,status,headers,config){
                console.log(data);
            });

    };

    $scope.addOrder = function () {
        if($scope.neworder.order_date.toISOString)
            $scope.neworder.order_date = $scope.neworder.order_date.toISOString();

        if($scope.newitems.length!=0) {
            if ($scope.curOrder.id) {

                deletepurchaseitems($scope.curOrder.id);
                $scope.neworder.$update(function (order) {
                    angular.extend($scope.curOrder, $scope.curOrder, order);
                    saveorderitems(order.id);
                });
            } else {
                $scope.neworder.$save(function (order) {
                    $scope.orders.push(order);
                    saveorderitems(order.id);
                });
            }
            $scope.orderedit = false;
            $scope.neworder = new Order();
        }else{
            alert('No Items in List');
        }
    };

    function saveorderitems(id){
        for(var i=0;i<$scope.newitems.length;i++)
        {
            $http.post('/api/orderitem',{orders_id:id,row_meterials_id:$scope.newitems[i].row_meterials_id,qty:$scope.newitems[i].qty,units_id:$scope.newitems[i].units_id,priority:$scope.newitems[i].priority,status:'0'}).
                success(function(data,status,headers,config){
                    //console.log(data);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
    }

    function deletepurchaseitems(id)
    {
        $http.delete('/api/orderitem/' + id)
            .success(function (data, status, headers) {
                //$scope.ServerResponse = data;
            })
            .error(function(data,status,headers,config){
                console.log(data);
            });
    }


    $scope.deleteOrder = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.orders.indexOf(item);
                $scope.orders.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelOrder = function () {
        $scope.orderedit = false;
        $scope.neworder = new Order();
    };

    $scope.addItem = function () {
        var row=$scope.newitems.length;
        //for(var i=0;i<row;i++) {
          //  if($scope.editingData[item.id] !=true) {
                $scope.inserted = {
                    id: row,
                    qty: '1',
                    priority: 'High'
                };
                $scope.newitems.push($scope.inserted);
                $scope.editingData[row] = true;
           // }
       // }
    };

    $scope.updateItem=function(item){
        if(item.row_meterials_id!=null) {
            if(item.units_id==null)
            {
                alert('Unit not selected');
                return;
            }
            includeunit(item);
        }
    }

    function includeunit(item){
        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                $scope.curunit=data;

                $http.get('/api/row_material/'+item.row_meterials_id).
                    success(function(data,status,headers,config){
                        $scope.currow=data;

                        updateRow(item);

                        //console.log(data);
                    }).error(function(data,status,headers,config){
                        console.log(data);
                    });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            qty:item.qty,
            units_id:item.units_id,
            unit:$scope.curunit.name,
            row_meterials_id:item.row_meterials_id,
            name:$scope.currow.name,
            priority:item.priority
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        //$scope.newitems[item.id].reload();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    }

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
                var curIndex = $scope.newitems.indexOf(item);
                $scope.newitems.splice(curIndex, 1);
        }
    };

    $scope.viewItem = function (item,size) {

        var thisItem={};
         $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                 thisItem=data;
                 var modalInstance = $modal.open({
                     templateUrl: 'template/viewproduct',
                     controller:'viewproductController',
                     size: size,
                     resolve: {
                         item: function () {
                             return thisItem;
                         }
                     }
                 });
               // console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewBranch = function (item,size) {

        var thisBranch={};
        $http.get('/api/branch/'+item.branches_id).
            success(function(data,status,headers,config){
                thisBranch=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewbranch',
                    controller:'viewbranchController',
                    size: size,
                    resolve: {
                        branch: function () {
                            return thisBranch;
                        }
                    }
                });
                // console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewOrderItem=function(order){

        $http.get('/api/orderitem/'+order.id).
            success(function(data,status,headers,config){
                //console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewitems',
                    controller:'viewitemController',
                    /*size: size,*/
                    resolve: {
                        order: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }
});

app.controller('viewitemController', function ($scope,$http,$modal, $modalInstance, order) {

    $scope.items = angular.copy(order);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.viewItem = function (item,size) {
        var thisItem={};
        $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };
});

app.controller('viewbranchController', function ($scope,$http, $modalInstance, branch) {

    $scope.branch=angular.copy(branch);


    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
});

