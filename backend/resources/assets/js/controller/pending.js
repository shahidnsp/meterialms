/**
 * Created by shahi on 04/06/16.
 */
app.controller('pendingorderController', function($scope,$http,$interval,$modal,$filter,Order){

    $scope.pendingorders = [];
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    loadOrder();
    function loadOrder(){
        $scope.pendingorders=[];
        Order.query(function(order){
            for(var i=0;i<order.length;i++)
            {
                var push=0;
                for(var j=0;j<order[i].order_item.length;j++)
                {
                    if(push==0)
                        if(order[i].order_item[j].status=="0")
                        {
                            $scope.pendingorders.push(order[i]);
                            push=1;
                        }
                }
            }
        });
    }
   //Put in interval, first trigger after 10 seconds
   var current= $interval(function(){
        loadOrder();
    }.bind(this), 20000);


    $scope.searchPendingDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/getorderwithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.pendingorders=[];
                var order=response.data;
                for(var i=0;i<order.length;i++)
                {
                    var push=0;
                    for(var j=0;j<order[i].order_item.length;j++)
                    {
                        if(push==0)
                            if(order[i].order_item[j].status=="0")
                            {
                                $scope.pendingorders.push(order[i]);
                                push=1;
                            }
                    }
                }
            });
        $interval.cancel(current);
    };

    $scope.assignOrder=function(order){

        $http.get('/api/orderitem/'+order.id).
            success(function(data,status,headers,config){
                //console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/assignorder',
                    controller:'assignorderController',
                    size: 'lg',
                    resolve: {
                        order: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewBranch = function (item,size) {
        var thisBranch={};
        $http.get('/api/branch/'+item.branches_id).
            success(function(data,status,headers,config){
                thisBranch=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewbranch',
                    controller:'viewbranchController',
                   /* size: size,
                    windowClass: 'my-modal-popup',*/
                    resolve: {
                        branch: function () {
                            return thisBranch;
                        }
                    }
                });
                // console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

});

app.controller('assignorderController', function ($scope,$http,$modal, $modalInstance, order,OrderItem) {


    $scope.items = angular.copy(order);
    var previous=angular.copy(order);
    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.viewItem = function (item,size) {
        var thisItem={};
        $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.changeAssign=function(item) {

        var qty=parseFloat(item.qty);
        var assign=parseFloat(item.assign);
        var bal=qty-assign;

        if(bal<0)
            alert('Assigned quantity is larger than requested');
        var newData={
            "id" :item.id,
            "qty": item.qty,
            "assign":item.assign,
            "balance":bal
        }

        angular.extend(item,item,newData);
    }
    $scope.changeBalance=function(item) {

        var qty=parseFloat(item.qty);
        var bal=parseFloat(item.balance);
        var assign=qty-bal;

        if(bal<0)
            alert('Assigned quantity is larger than requested');
        var newData={
            "id" :item.id,
            "qty": item.qty,
            "assign":assign
        }

        angular.extend(item,item,newData);
    }

    $scope.assignOrder=function() {

        var confirmDelete = confirm("Do you really need to assign order ?");
        if (confirmDelete) {
            for (var i = 0; i < $scope.items.length; i++) {
                $http.put('/api/orderitem/' + $scope.items[i].id, {
                    orders_id: $scope.items[i].orders_id,
                    row_meterials_id: $scope.items[i].row_meterials_id,
                    qty: $scope.items[i].qty,
                    units_id: $scope.items[i].units_id,
                    balance: $scope.items[i].balance,
                    assign: $scope.items[i].assign,
                    previous:previous[i].assign
                }).
                    success(function (data, status, headers, config) {
                        //console.log(data);
                    }).error(function (data, status, headers, config) {
                        console.log(data);
                    });
            }
            $modalInstance.dismiss('Close');
        }
    }


});
