/**
 * Created by shahi on 04/06/16.
 */
app.controller('saleController', function($scope,$http,$modal,$filter,Sale,Row,Unit,SaleItem){
    $scope.saleedit = false;
    $scope.newsale = {};
    $scope.curSale = {};
    $scope.sales = [];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.rows = [];
    $scope.currow = {};
    $scope.units = [];
    $scope.curunit = {};
    $scope.editingData=[];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    Sale.query(function(sale){
        $scope.sales = sale;
    });
    Row.query(function(row){
        $scope.rows = row;
    });
    Unit.query(function(unit){
        $scope.units = unit;
    });

    $scope.searchSalesDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

       /* $http.get('/api/getsaleswithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.sales=response.data;
            });*/
        Sale.query({fromDate:from,toDate:to},function(sale){
            $scope.sales = sale;
        });
    };

    //$scope.newitems=new SaleItem();

    $scope.newSale = function () {
        $scope.saleedit = true;
        $scope.newsale = new Sale();
        $scope.newsale.date = new Date();
        $scope.curSale = {};
        $scope.newitems = [];
    };

    $scope.editSale = function (thisSale) {
        $scope.saleedit = true;
        $scope.curSale =  thisSale;
        $scope.newsale = angular.copy(thisSale);

        $http.get('/api/sale_item/'+thisSale.id).
            success(function(data,status,headers,config){
                $scope.newitems=[];
                for(var i=0;i<data.length;i++)
                {
                    $scope.updateitem={
                        id:data[i].id,
                        qty:data[i].qty,
                        units_id:data[i].units_id,
                        unit:data[i].unit.name,
                        row_meterials_id:data[i].row_meterials_id,
                        name:data[i].row_meterial.name
                    };

                    $scope.newitems.push($scope.updateitem);

                    $scope.editingData[data[i].id] = false;
                }

            }).error(function(data,status,headers,config){
                console.log(data);
            });

    };

    $scope.addSale = function () {
        if($scope.newsale.date.toISOString)
            $scope.newsale.date = $scope.newsale.date.toISOString();

        if($scope.newitems.length!=0) {
            if ($scope.curSale.id) {

                deletepurchaseitems($scope.curSale.id);
                $scope.newsale.$update(function (sale) {
                    angular.extend($scope.curSale, $scope.curSale, sale);
                    savesaleitems(sale.id);
                });
            } else {
                $scope.newsale.$save(function (sale) {
                    $scope.sales.push(sale);
                    savesaleitems(sale.id);
                });
            }
            $scope.saleedit = false;
            $scope.newsale = new Sale();
        }else{
            alert('No Items in List');
        }
    };

    function savesaleitems(id){
        for(var i=0;i<$scope.newitems.length;i++)
        {
            $http.post('/api/sale_item',{sales_id:id,row_meterials_id:$scope.newitems[i].row_meterials_id,qty:$scope.newitems[i].qty,units_id:$scope.newitems[i].units_id}).
                success(function(data,status,headers,config){
                    //console.log(data);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
    }

    function deletepurchaseitems(id)
    {
        $http.delete('/api/sale_item/' + id)
            .success(function (data, status, headers) {
                //$scope.ServerResponse = data;
            })
            .error(function(data,status,headers,config){
                console.log(data);
            });
    }


    $scope.deleteSale = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            deletepurchaseitems(item.id);
            item.$delete(function(){
                var curIndex = $scope.sales.indexOf(item);
                $scope.sales.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelSale = function () {
        $scope.saleedit = false;
        $scope.newsale = new Sale();
    };

    $scope.addItem = function () {
        var row=$scope.newitems.length;
        //for(var i=0;i<row;i++) {
        //  if($scope.editingData[item.id] !=true) {
        $scope.inserted = {
            id: row,
            qty: '1'
        };
        $scope.newitems.push($scope.inserted);
        $scope.editingData[row] = true;
        // }
        // }
    };

    $scope.updateItem=function(item){
        if(item.row_meterials_id!=null) {
            if(item.units_id==null)
            {
                alert('Unit not selected');
                return;
            }
            includeunit(item);
        }
    }

    function includeunit(item){
        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                $scope.curunit=data;

                $http.get('/api/row_material/'+item.row_meterials_id).
                    success(function(data,status,headers,config){
                        $scope.currow=data;

                        updateRow(item);

                        //console.log(data);
                    }).error(function(data,status,headers,config){
                        console.log(data);
                    });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            qty:item.qty,
            units_id:item.units_id,
            unit:$scope.curunit.name,
            row_meterials_id:item.row_meterials_id,
            name:$scope.currow.name
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        //$scope.newitems[item.id].reload();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    }

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
            var curIndex = $scope.newitems.indexOf(item);
            $scope.newitems.splice(curIndex, 1);
        }
    };

    $scope.viewItem = function (item,size) {

        var thisItem={};
        $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                // console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewSaleItem=function(sale){

        $http.get('/api/sale_item/'+sale.id).
            success(function(data,status,headers,config){
                //console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewsaleitems',
                    controller:'viewsaleitemController',
                    /*size: size,*/
                    resolve: {
                        sale: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }



});

app.controller('viewsaleitemController', function ($scope,$http,$modal, $modalInstance, sale) {

    $scope.items = angular.copy(sale);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.viewItem = function (item,size) {
        var thisItem={};
        $http.get('/api/row_material/'+item.row_meterials_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };




});

