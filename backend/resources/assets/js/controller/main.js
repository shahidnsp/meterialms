/**
 * Created by shahi on 31/5/16.
 */
app.controller('mainController', function($scope,$http,MainCategory){
    $scope.mainedit = false;
    $scope.newmain = {};
    $scope.curMain = {};
    $scope.mains = [];

    MainCategory.query(function(main){
        $scope.mains = main;
    });

    $scope.newMain = function () {
        $scope.mainedit = true;
        $scope.newmain = new MainCategory();
        $scope.newmain.date = new Date();
        $scope.curMain = {};
    };
    $scope.editMain = function (thisMain) {
        $scope.mainedit = true;
        $scope.curMain =  thisMain;
        $scope.newmain = angular.copy(thisMain);
    };
    $scope.addMain = function () {
        if ($scope.curMain.id) {
            $scope.newmain.$update(function(main){
                angular.extend($scope.curMain, $scope.curMain, main);
            });
        } else{
            $scope.newmain.$save(function(main){
                $scope.mains.push(main);
            });
        }
        $scope.mainedit = false;
        $scope.newmain = new MainCategory();
    };
    $scope.deleteMain = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.mains.indexOf(item);
                $scope.mains.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelMain = function () {
        $scope.mainedit = false;
        $scope.newmain = new MainCategory();
    };
});
