/**
 * Created by shahi on 01/06/16.
 */
app.controller('vendorController', function($scope,$http,Vendor){
    $scope.vendoredit = false;
    $scope.newvendor = {};
    $scope.curVendor = {};
    $scope.vendors = [];

    Vendor.query(function(vendor){
        $scope.vendors = vendor;
    });

    $scope.newVendor = function () {
        $scope.vendoredit = true;
        $scope.newvendor = new Vendor();
        $scope.newvendor.date = new Date();
        $scope.curVendor = {};
    };
    $scope.editVendor = function (thisVendor) {
        $scope.vendoredit = true;
        $scope.curVendor =  thisVendor;
        $scope.newvendor = angular.copy(thisVendor);
    };
    $scope.addVendor = function () {
        if ($scope.curVendor.id) {
            $scope.newvendor.$update(function(vendor){
                angular.extend($scope.curVendor, $scope.curVendor, vendor);
            });
        } else{
            $scope.newvendor.$save(function(vendor){
                $scope.vendors.push(vendor);
            });
        }
        $scope.vendoredit = false;
        $scope.newvendor = new Vendor();
    };
    $scope.deleteVendor = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.vendors.indexOf(item);
                $scope.vendors.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelVendor = function () {
        $scope.vendoredit = false;
        $scope.newvendor = new Vendor();
    };
});
