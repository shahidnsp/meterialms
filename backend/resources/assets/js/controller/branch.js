/**
 * Created by shahi on 01/06/16.
 */
app.controller('branchController', function($scope,$http,Branch){
    $scope.branchedit = false;
    $scope.newbranch = {};
    $scope.curBranch = {};
    $scope.branchs = [];

    Branch.query(function(branch){
        $scope.branchs = branch;
    });

    $scope.newBranch = function () {
        $scope.branchedit = true;
        $scope.newbranch = new Branch();
        $scope.newbranch.date = new Date();
        $scope.curBranch = {};
    };
    $scope.editBranch = function (thisBranch) {
        $scope.branchedit = true;
        $scope.curBranch =  thisBranch;
        $scope.newbranch = angular.copy(thisBranch);
    };
    $scope.addBranch = function () {
        if ($scope.curBranch.id) {
            $scope.newbranch.$update(function(branch){
                angular.extend($scope.curBranch, $scope.curBranch, branch);
            });
        } else{
            $scope.newbranch.$save(function(branch){
                $scope.branchs.push(branch);
            });
        }
        $scope.branchedit = false;
        $scope.newbranch = new Branch();
    };
    $scope.deleteBranch = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.branchs.indexOf(item);
                $scope.branchs.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelBranch = function () {
        $scope.branchedit = false;
        $scope.newbranch = new Branch();
    };
});
