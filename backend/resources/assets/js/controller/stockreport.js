/**
 * Created by shahi on 31/5/16.
 */
app.controller('stockreportController', function($scope,$http,$modal){

    $scope.stocks=[];
    $http.get('/api/stockreport')
        .then(function(response){
            $scope.stocks=response.data;
        });

    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproduct',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });
    };
});
