/**
 * Created by shahi on 04/06/16.
 */
app.controller('completedorderController', function($scope,$http,$modal,$filter,Order){

    $scope.pendingorders = [];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    Order.query(function(order){
        for(var i=0;i<order.length;i++)
        {
            var push=0;
            for(var j=0;j<order[i].order_item.length;j++)
            {
                if(push==0)
                    if(order[i].order_item[j].status=="1")
                    {
                        $scope.pendingorders.push(order[i]);
                        push=1;
                    }
            }
        }
    });

    $scope.searchCompletedDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/getorderwithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                var order=response.data;
                $scope.pendingorders=[];
                //console.log(response.data);
                for(var i=0;i<order.length;i++)
                {
                    var push=0;
                    for(var j=0;j<order[i].order_item.length;j++)
                    {
                        if(push==0)
                            if(order[i].order_item[j].status=="1")
                            {
                                $scope.pendingorders.push(order[i]);
                                push=1;
                            }
                    }
                }
            });
    };

    $scope.assignOrder=function(order){

        $http.get('/api/orderitem/'+order.id).
            success(function(data,status,headers,config){
                //console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/assignorder',
                    controller:'assignorderController',
                    /*size: size,*/
                    resolve: {
                        order: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewBranch = function (item,size) {
        var thisBranch={};
        $http.get('/api/branch/'+item.branches_id).
            success(function(data,status,headers,config){
                thisBranch=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewbranch',
                    controller:'viewbranchController',
                    /*size: size,*/
                    resolve: {
                        branch: function () {
                            return thisBranch;
                        }
                    }
                });
                // console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

});