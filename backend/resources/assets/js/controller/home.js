app.controller('homecontroller', function($scope,$http,$location,$interval,$modal,webNotification,Message,Task,UserInfo){

    //loadNotification();

    $scope.messages=[];
    $scope.tasks=[];
    $scope.newtask={};
    $scope.newmessage=new Message();

    $scope.newtask=new Task();
    $scope.newtask.fromDate=new Date();
    $scope.newtask.toDate=new Date();

    $scope.materialcount=0;
    $scope.pndngOrder=0;
    $scope.cmptOrder=0;
    $scope.usercount=0;
    $scope.branchcount=0;
    $scope.reminders=[];

    $scope.user = {};

    UserInfo.query().success(function(data){
        $scope.user = data;
        console.log(data);
    });

    //Put in interval, first trigger after 3 seconds
    $interval(function(){
        loadMessage();
    }.bind(this), 3000);

    loadMessage();
    function loadMessage() {
        Message.query(function (message) {
            $scope.messages = message;
        });
    }

    $scope.$on('$destroy', function() {
        $scope.stop();
    });

    loadTask();
    function loadTask() {
        Task.query(function (task) {
            $scope.tasks = task;
        });
    }

    $scope.deleteMessage = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.messages.indexOf(item);
                $scope.messages.splice(curIndex, 1);
            });
            loadMessage();
        }
    };

    $scope.addMessage = function () {
            $scope.newmessage.$save(function(main){
                $scope.messages.push(main);
            });
        $scope.newmessage = new Message();
        loadMessage();
    };

    $scope.addTask = function () {
        if($scope.newtask.fromDate.toISOString)
            $scope.newtask.fromDate = $scope.newtask.fromDate.toISOString();
        if($scope.newtask.toDate.toISOString)
            $scope.newtask.toDate = $scope.newtask.toDate.toISOString();


        $scope.newtask.$save(function(main){
            $scope.tasks.push(main);
        });
        $scope.newtask = new Task();
        $scope.newtask.fromDate=new Date();
        $scope.newtask.toDate=new Date();
        loadTask();
    };

    $scope.deleteTask = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.tasks.indexOf(item);
                $scope.tasks.splice(curIndex, 1);
            });
            loadTask();
        }
    };

    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
	$scope.maxSize = 5;


    $scope.exportToExcel=function(tableId){ // ex: '#my-table'
        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById(tableId); // id of table

        for(j = 0 ; j < tab.rows.length ; j++)
        {
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

    $scope.printReport=function(tableId) {
        var divToPrint=document.getElementById(tableId);
        newWin=window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }


	//// Toggle side menu
	$scope.menuStatus = true;
	$scope.toggleMenu = function(){
		$scope.menuStatus = $scope.menuStatus === false ? true : false;
	};


    function loadNotification()
    {
        webNotification.showNotification('Example Notification', {
            body: 'Notification Text...',
            icon: '../bower_components/HTML5-Desktop-Notifications2/alert.ico',
            onClick: function onNotificationClicked() {
                console.log('Notification clicked.');
            },
            autoClose: 4000 //auto close the notification after 4 seconds (you can manually close it via hide function)
        }, function onShow(error, hide) {
            if (error) {
                window.alert('Unable to show notification: ' + error.message);
            } else {
                console.log('Notification Shown.');

                setTimeout(function hideNotification() {
                    console.log('Hiding notification....');
                    hide(); //manually close the notification (you can skip this if you use the autoClose option)
                }, 5000);
            }
        });
    }

    $scope.go = function ( path ) {
        $location.path( path );
    };

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

    $scope.myJson={};

    //Put in interval, first trigger after 10 seconds
    $interval(function(){
        loadGraph();
    }.bind(this), 10000);
    loadGraph();

    function loadGraph()
    {
        var newcount=0;
        var pendingcount=0;

        $http.get('/api/pendingcount').
            success(function(data,status,headers,config){
                $scope.cmptOrder=data;
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
        $http.get('/api/newcount').
            success(function(data1,status,headers,config){
                $scope.pndngOrder=data1;
            }).error(function(data1,status,headers,config){
                console.log(data1);
            });
        graph($scope.cmptOrder,$scope.pndngOrder);

        $http.get('/api/rowmaterialcount').
            success(function(data1,status,headers,config){
                $scope.materialcount=data1;

            }).error(function(data1,status,headers,config){
                console.log(data1);
            });
        $http.get('/api/usercount').
            success(function(data1,status,headers,config){
                $scope.usercount=data1;

            }).error(function(data1,status,headers,config){
                console.log(data1);
            });
        $http.get('/api/branchcount').
            success(function(data1,status,headers,config){
                $scope.branchcount=data1;

            }).error(function(data1,status,headers,config){
                console.log(data1);
            });
    }



    function graph(value1,value2){
        $scope.config = {
            title: 'Order Status',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                //could be 'left, right'
                position: 'right'
            }
        };

        $scope.data = {
            series: ['Completed', 'Pending'],
            data: [{
                x: "Completed",
                y: [value1],
                tooltip: "Completed Order "+value1
            }, {
                x: "Pending",
                y: [value2],
                tooltip: "Pending Order "+value2
            }]
        };

        /*var branches=[];
        $http.get('/api/getbranches').
            success(function(data1,status,headers,config){
                var branch=data1;
                for(var i=0;i<branch.length;i++){
                    $http.get('/api/getbranchusercount/'+branch[i].id).
                        success(function(data2,status,headers,config){
                            var each={
                                'name':branch[i].name,
                                'count':data2
                            }
                            branches=branches.concat(each);
                        }).error(function(data2,status,headers,config){
                            console.log(data2);
                        });
                }

            }).error(function(data1,status,headers,config){
                console.log(data1);
            });

        $scope.config2 = {
            title: 'Branches',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                //could be 'left, right'
                position: 'right'
            }
        };

        $scope.data2 = {
            series: ['Completed', 'Pending']
            *//*data: [{
                x: "Branch 1",
                y: [100, 500],
                tooltip: "this is tooltip"
            }, {
                x: "Branch 2",
                y: [300, 100]
            }, {
                x: "Branch 3",
                y: [351,5]
            }, {
                x: "Branch 4",
                y: [54, 2]
            }]*//*
        };

        for(var i=0;i<branches.length;i++){
            var each={
                x:branches[i].name,
                y:branches[i].count
            };
            $scope.data2.data.concat(each);
        }*/
    }

    $http.get('/api/warningreport').
        success(function(data,status,headers,config){
            $scope.reminders=data;
            //console.log(data);
        }).error(function(data,status,headers,config){
            console.log(data);
        });


    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproduct',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });
    };

});