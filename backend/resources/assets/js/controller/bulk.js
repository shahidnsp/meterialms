/**
 * Created by shahi on 01/06/16.
 */
app.controller('bulkController', function($scope,$http,$modal,Row,Unit,MainCategory,Storage,Vendor,SubCategory){
    $scope.rows = [];
    $scope.units=[];
    $scope.maincategories=[];
    $scope.storages=[];
    $scope.vendors=[];
    $scope.subcategories=[];


    loadRow();

    function loadRow(){
        Row.query(function(row){
            $scope.rows = row;
        });
    }


    Unit.query(function(unit){
        $scope.units = unit;
    });

    MainCategory.query(function(main){
        $scope.maincategories = main;
    });

    Storage.query(function(store){
        $scope.storages = store;
    });

    Vendor.query(function(vendor){
        $scope.vendors = vendor;
    });

    SubCategory.query(function(sub){
        $scope.subcategories = sub;
    });

    $scope.updateItem = function (item) {
        var uri ='api/row_material/'+item.id;

        $http.put(uri, item,{headers: '{"Content-Type": "application/json;charset=UTF-8"}'} )
            .success(function (data, status, headers, config) {
                alert('Updated Successfully.');
            })
            .error(function (data, status, header, config) {
                alert('Server error');
                console.log(data);
            });
    };

    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproduct',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };

    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});

app.controller('viewphotoController', function ($scope, $modalInstance, item) {

    $scope.product = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
});





