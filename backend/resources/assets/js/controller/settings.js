app.controller('settingsController', function($scope,$http,User,UserInfo,Branch){
    $scope.resetPermission = false;

    $scope.useredit = false;
    $scope.newuser = {};
    $scope.curUser = {};
    $scope.users = [];
    $scope.branches = [];



    Branch.query(function(branch){
        $scope.branches = branch;
    });

    //Only for admin load all users
    if($scope.user.isAdmin=='A'){
        User.query(function(users){
            $scope.users =users;
        });
    }


    $scope.newUser = function (argument) {
        $scope.useredit = true;
        $scope.newuser = new User();
        $scope.newuser.isAdmin='u';
        $scope.curUser = {};
    };

    $scope.editUser = function (thisUser) {
        $scope.useredit = true;
        $scope.curUser =  thisUser;
        $scope.newuser = angular.copy(thisUser);
    };

    $scope.addUser = function (argument) {
        if ($scope.curUser.id) {
            $scope.newuser.$update(function(user){
                angular.extend($scope.curUser, $scope.curUser, user);
            });
        } else{
            $scope.newuser.$save(function(user){
                $scope.users.push(user);
            });
        }
        $scope.useredit = false;
        $scope.newuser = new User();
    };

    $scope.deleteUser = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.users.indexOf(item);
                $scope.users.splice(curIndex, 1);
            });
        }
    };

    $scope.cancelUser = function (argument) {
        $scope.useredit = false;
        $scope.newuser = new User();
    };
    $scope.resetPassword = function (thisUser) {
        var confirmReset = confirm("Do you need to reset "+thisUser.name+"\'s password !");
        if (confirmReset) {
            UserInfo.resetPassword(thisUser.id);
        }
    };
    $scope.breadCrumbs = [];
    $scope.userPermission = {
        "write": true,
        "edit": false
    };

    $scope.pages = [];
    $scope.mypermisions = [];
    $scope.permissionMode = function (thisUser) {
        $scope.curUser = thisUser;

        UserInfo.getAllPage(thisUser.id).success(function(data){
            $scope.pages = data
        });

        UserInfo.getUserPermission(thisUser.id).success(function(data){
            $scope.mypermisions = data;
        });

        $scope.resetPermission = true;
        $scope.breadCrumbs.push("home", $scope.curUser.name);
    };

    $scope.backToUsers = function () {
        $scope.breadCrumbs = [];
        $scope.pages=[];
        $scope.mypermisions = [];
        $scope.resetPermission = false;
    };

    $scope.savePermission = function (){
        UserInfo.setUserPermission({id:$scope.curUser.id,permission:$scope.mypermisions})
            .success(function(data){
                alert('User Permission updated');
            });
    };

    $scope.editPage = false;
    $scope.writePage= true;

    $scope.toggleWrite = function(page){
        page.write = page.write =='true'?'fasle':'true';
    };

    $scope.toggleEdit = function(page){
        page.edit = page.edit=='true'?'false':'true';
    };

    $scope.addToList = function (thisPage) {
        // add to premission list
        $scope.mypermisions.push(thisPage);
        // delete from list
        var curIndex = $scope.pages.indexOf(thisPage);
        $scope.pages.splice(curIndex,1);
    };

    $scope.removeToList = function (thisItem) {
        // add to premission list
        $scope.pages.push(thisItem);
        // delete from list
        var curIndex = $scope.mypermisions.indexOf(thisItem);
        $scope.mypermisions.splice(curIndex, 1);
    };

    //Get current user details
    $scope.userdetails={};

    $scope.password={};

    $scope.changePassword = function(){
        if($scope.password.newpassword === $scope.password.password)
            UserInfo.changePassword($scope.password.oldPassword,$scope.password.password,$scope.password.username);
        //UserInfo.changePassword($scope.password.newpassword,$scope.password.oldPassword);
        else
            alert('Passwords doesn\'t match !!!');
    };

    UserInfo.get($scope.user.id).success(function(user){
        $scope.userdetails = user;
    });

    $scope.inputs = {};

    $scope.editDetail = function (key, thisDetail) {
        $scope.inputs[key] = thisDetail;
        console.log(key);
    };

    $scope.saveDetail = function (key,thisResult) {
        $scope.userdetails[key] = thisResult;
        UserInfo.save($scope.user.id,$scope.userdetails).success(function(data){
            alert('Personal Details has been updated, please refresh.');
        });
    };
});