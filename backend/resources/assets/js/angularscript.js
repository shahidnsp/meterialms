var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'angular-web-notification',
        'angularCharts',
        'UnitService',
        'MainService',
        'StorageService',
        'SubService',
        'BranchService',
        'VendorService',
        'MessageService',
        'RowService',
        'OrderService',
        'OrderitemService',
        'PurchaseService',
        'PurchaseitemService',
        'TaskService',
        'UserInfoService',
        'UserService',
        'SaleService',
        'SaleitemService',
        'PurchaseOrder',
    ]);
	app.config(function($routeProvider, $locationProvider) {
		// $locationProvider.html5Mode(true);
		$routeProvider
        .when('/', {
            templateUrl: 'template/dashboard'
        })
		.when('/dashboard', {
			templateUrl: 'template/dashboard'
		})
        .when('/unit', {
            templateUrl: 'template/unit',
            controller:'unitController'
        })
        .when('/maincategory', {
            templateUrl: 'template/maincategory',
            controller:'mainController'
        })
        .when('/storagelocation', {
            templateUrl: 'template/storagelocation',
            controller:'storageController'
        })
        .when('/subcategory', {
            templateUrl: 'template/subcategory',
            controller:'subController'
        })
        .when('/branch', {
            templateUrl: 'template/branch',
            controller:'branchController'
        })
        .when('/vendor', {
            templateUrl: 'template/vendor',
            controller:'vendorController'
        })
        .when('/rowmaterial', {
            templateUrl: 'template/rowmaterial',
            controller:'rowController'
        })
        .when('/bulkupdate', {
            templateUrl: 'template/bulkupdate',
            controller:'bulkController'
        })
        .when('/tracking', {
            templateUrl: 'template/tracking',
            controller:'trackingController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller:'profileController'
        })
        .when('/order', {
            templateUrl: 'template/order',
            controller:'orderController'
        })
        .when('/purchase', {
            templateUrl: 'template/purchase',
            controller:'purchaseController'
        })
        .when('/pendingorder', {
            templateUrl: 'template/pendingorder',
            controller:'pendingorderController'
        })
        .when('/completedorder', {
            templateUrl: 'template/completedorder',
            controller:'completedorderController'
        })
        .when('/settings', {
            templateUrl: 'template/settings',
            controller:'settingsController'
        })
        .when('/sale', {
            templateUrl: 'template/sale',
            controller:'saleController'
        })

        .when('/stockreport', {
            templateUrl: 'template/stockreport',
            controller: 'stockreportController'
        })

        .when('/purchase-order', {
            templateUrl: 'template/purchaseOrder',
            controller:'purchaseorderController'
        })
		.otherwise({
			redirectTo: 'template/dashboard'
		});
	});
	app.filter('pagination', function() {
	  return function(input, currentPage, pageSize) {
		if(angular.isArray(input)) {
		  var start = (currentPage-1)*pageSize;
		  var end = currentPage*pageSize;
		  return input.slice(start, end);
		}
	  };
	});
	app.filter('percentage', ['$filter', function ($filter) {
		return function (input, decimals) {
			return $filter('number')(input * 100, decimals) + '%';
		};
	}]);
	app.filter('sum', function(){
		return function(items, prop){
			return items.reduce(function(a, b){
				return a + b[prop];
			}, 0);
		};
	});