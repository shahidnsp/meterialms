/**
 * Created by shahi on 4/6/16.
 */
angular.module('OrderitemService',[]).factory('OrderItem',['$resource',
    function($resource){
        return $resource('/api/orderitem/:itemId',{
            itemId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);