/**
 * Created by shahi on 01/6/16.
 */
angular.module('TaskService',[]).factory('Task',['$resource',
    function($resource){
        return $resource('/api/task/:taskId',{
            taskId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);