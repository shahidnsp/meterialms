/**
 * Created by shahi on 31/5/16.
 */
angular.module('MessageService',[]).factory('Message',['$resource',
    function($resource){
        return $resource('/api/message/:messageId',{
            messageId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);