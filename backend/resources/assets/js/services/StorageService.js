/**
 * Created by shahi on 31/5/16.
 */
angular.module('StorageService',[]).factory('Storage',['$resource',
    function($resource){
        return $resource('/api/location/:storageId',{
            storageId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);