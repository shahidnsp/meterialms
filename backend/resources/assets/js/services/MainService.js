/**
 * Created by shahi on 31/5/16.
 */
angular.module('MainService',[]).factory('MainCategory',['$resource',
    function($resource){
        return $resource('/api/main_categories/:mainId',{
            mainId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);