/**
 * Created by shahi on 4/6/16.
 */
angular.module('SaleitemService',[]).factory('SaleItem',['$resource',
    function($resource){
        return $resource('/api/sale_item/:itemId',{
            itemId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);