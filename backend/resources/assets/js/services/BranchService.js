/**
 * Created by shahi on 31/5/16.
 */
angular.module('BranchService',[]).factory('Branch',['$resource',
    function($resource){
        return $resource('/api/branch/:branchId',{
            branchId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);