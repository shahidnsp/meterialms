/**
 * Created by shahi on 4/6/16.
 */
angular.module('OrderService',[]).factory('Order',['$resource',
    function($resource){
        return $resource('/api/order/:orderId',{
            orderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);