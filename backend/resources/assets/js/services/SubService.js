/**
 * Created by shahi on 31/5/16.
 */
angular.module('SubService',[]).factory('SubCategory',['$resource',
    function($resource){
        return $resource('/api/sub_categories/:subId',{
            subId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);