/**
  Created by Noushid 01-06-2016
 **/

angular.module('RowService',[]).factory('Row',['$resource',
    function($resource){
        return $resource('/api/row_material/:rowId',{
            rowId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);

