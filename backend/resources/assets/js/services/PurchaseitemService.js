/**
 * Created by shahi on 4/6/16.
 */
angular.module('PurchaseitemService',[]).factory('PurchaseItem',['$resource',
    function($resource){
        return $resource('/api/purchase_item/:itemId',{
            itemId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);