/**
 * Created by noushi on 26/9/16.
 */
/**
 * Created by shahi on 4/6/16.
 */
angular.module('PurchaseOrder',[]).factory('PurchaseOrder',['$resource',
    function($resource){
        return $resource('/api/purchase_order/:purchaseOrderId',{
            purchaseOrderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);