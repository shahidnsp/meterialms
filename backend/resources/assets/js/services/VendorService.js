/**
 * Created by shahi on 31/5/16.
 */
angular.module('VendorService',[]).factory('Vendor',['$resource',
    function($resource){
        return $resource('/api/vender/:venderId',{
            venderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);