/**
 * Created by Shahid on 9/6/16.
 */
angular.module('UserService',[]).factory('User',['$resource',
    function($resource){
        return $resource('/api/user/:userId',{
            userId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);