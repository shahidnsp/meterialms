/**
 * Created by shahi on 11/6/16.
 */
angular.module('SaleService',[]).factory('Sale',['$resource',
    function($resource){
        return $resource('/api/sale/:saleId',{
            saleId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);