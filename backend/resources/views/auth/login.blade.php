<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>

	<style>
		.login-form{
			position: relative;
		}
		.login-form i{
			color: #888;
			position: absolute;
			top: 10px;
			right: 10px;
		}
		.login-form i + .form-control{
			padding-right: 30px;
		}
	</style>
	<link rel="stylesheet" href="[[ elixir("css/app.css") ]]">
</head>
<body>
	<div class="container-fluid">
		<div class="row ">
			@if (count($errors) > 0)
			<div class="col-md-4 col-md-offset-4">
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<p>[[$error]] </p>
					@endforeach
				</div>
			</div>
			@endif
			<div class="col-md-4 col-md-offset-4">
				<form method="post" action="[[URL::route('login')]]">
					<h4><i class="fa fa-user"></i> Login</h4>
					<input type="hidden" name="_token" value="{!!csrf_token()!!}">
					<div class="form-group login-form">
						<i class="fa fa-user fa-lg" style="position:absolute;"></i>
						<input type="text" class="form-control" name="username" placeholder="Username">
					</div>
					<div class="form-group login-form">
						<i class="fa fa-lock fa-lg" style="position:absolute;"></i>
						<input type="password" class="form-control" name="password" placeholder="Password">
					</div>

					<div  class="checkbox pull-left">
						<label><input type="checkbox"> Remember me</label>
					</div>
					<a class="pull-right" href="#">Forgot password ?</a>
					<button type="submit" class="btn btn-primary btn-block">Login</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>