<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Purchases </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div cgitlass="box">
            <button ng-if="user.permissions.purchase.write==='true'" ng-click="newPurchase();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Purchase</button>
            <form class="form-horizontal" ng-show="purchaseedit" ng-submit="addPurchase();">
                <h3>New Purchase</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Purchase Date</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newpurchase.date" is-open="purchase_datepicker" show-button-bar="false" show-weeks="false" readonly>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" ng-click="purchase_datepicker=true"><i class="fa fa-calendar"></i></button>
								</span>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" ng-model="newpurchase.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div>
                    <div class="col-lg-13">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bpurchaseed table-hover" id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SlNo</th>
                                            <th>Row Meterial</th>
                                            <th>Qty</th>
                                            <th>Unit</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="newitem in newitems">
                                            <td><div>{{$index+1}}</div></td>
                                            <td>
                                                <div ng-hide="editingData[newitem.id]">{{newitem.name}}</div>
                                                <div ng-show="editingData[newitem.id]"><select class="form-control" ng-model="newitem.row_meterials_id" > <option ng-repeat="row in rows" value="{{row.id}}" ng-selected="newitem.row_meterials_id == row.id">{{row.name}}</option></select></div>
                                            </td>
                                            <td>
                                                <div ng-hide="editingData[newitem.id]">{{newitem.qty}}</div>
                                                <div ng-show="editingData[newitem.id]"><input type="text" onkeypress="return isNumberKey(event)" class="form-control" ng-model="newitem.qty"></td></div>
                                <td>
                                    <div ng-hide="editingData[newitem.id]">{{newitem.unit}}</div>
                                    <div ng-show="editingData[newitem.id]"><select class="form-control" ng-model="newitem.units_id"> <option ng-repeat="unit in units" value="{{unit.id}}">{{unit.name}}</option></select></div>
                                </td>

                                <td ng-show="newitems.length>0">
                                    <div ng-hide="editingData[newitem.id]" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-success" ng-click="editItem(newitem);">Edit
                                        </button>
                                        <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="viewItem(newitem); editmode = !editmode">View
                                        </button>
                                    </div>
                                    <div ng-show="editingData[newitem.id]"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-success" ng-click="updateItem(newitem);">Update
                                        </button>
                                        <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="viewItem(newitem); editmode = !editmode">View
                                        </button>
                                    </div>
                                </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12 text-right">
                <button type="button" class="btn btn-default" ng-click="addItem();">Add Item</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12 text-right">
                <button type="button" class="btn btn-default" ng-click="cancelPurchase();">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
        <hr>
        </form>

        <h3>Purchase and details</h3>
        <div class="row">
            <div class="col-md-4">
                <label for="">Show
                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                    entries
                </label>
            </div>
            <div class="col-sm-3 text-center">
                <div>
                    <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tablePurchase');">Export</button>
                    <button class="btn btn-primary btn-sm" ng-click="printReport('tablePurchase');">Print</button>
                </div>
            </div>
            <div class="col-md-5 text-right">
                <div class="form-inline form-group">
                    <label for="filter-list">Search </label>
                    <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">From</label>
                    <div class="col-sm-4 input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <label for="" class="col-sm-1 control-label">To</label>
                    <div class="col-sm-4 input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <button class="col-sm-1 btn btn-warning btn-sm" ng-click="searchPurchaseDate(fromDate,toDate);">Search</button>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-primary">Purchases and Details from {{fromDate | date:'dd-MMMM-yyyy'}} to {{toDate | date:'dd-MMMM-yyyy'}}</p>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="tablePurchase">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Item</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="purchase in listCount  = (purchases | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{purchase.date | date:'dd-MMMM-yyyy'}}</td>
                                <td><p class="description" popover="{{purchase.description}}" popover-trigger="mouseenter">{{purchase.description}}</p></td>
                                <td> <button type="button" class="btn btn-default" ng-click="viewPurchaseItem(purchase);">
                                        View Purchase Items
                                    </button></td>
                                <td>
                                    <div  ng-if="user.permissions.purchase.edit==='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-default" ng-click="editPurchase(purchase);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deletePurchase(purchase); editmode = !editmode">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix" ng-show="purchases.length > numPerPage">
            <pagination
                ng-model="currentPage"
                total-items="listCount.length"
                max-size="maxSize"
                items-per-page="numPerPage"
                boundary-links="true"
                class="pagination-sm pull-right"
                previous-text="&lsaquo;"
                next-text="&rsaquo;"
                first-text="&laquo;"
                last-text="&raquo;"
                ></pagination>
        </div>
    </div>
</div>
</div>

