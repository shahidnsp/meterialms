<button type="button" class="close" ng-click="cancel();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <h3 class="modal-title">Branch Name:{{branch.name}}</h3>
</div>
<div class="modal-body">

    <div class="container">
        <div class="row">

            <div class="toppad" >


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{branch.name}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                           <!-- <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" ng-click="viewPhoto(branch);" ng-src="{{branch.photo}}"  style="width: 500px;height: 120px" class="img-circle img-responsive"> </div>-->
                            <div class=" col-md-9 col-lg-12 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Address:</td>
                                        <td>{{branch.address}}</td>
                                    </tr>
                                    <tr>
                                        <td>Place:</td>
                                        <td>{{branch.place}}</td>

                                    </tr>
                                    <tr>
                                        <td>Phone:</td>
                                        <td>{{branch.phone}}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Mobile:</td>
                                        <td>{{branch.mobile}}</td>
                                    </tr>
                                    <tr>
                                        <td>Supervisor:</td>
                                        <td>{{branch.supervisor}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



