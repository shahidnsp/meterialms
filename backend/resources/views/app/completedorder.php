<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Assigned Orders </h2>
    </div>
</div>
<div class="box">
    <h3>Assigned Orders and details</h3>
    <div class="row">
        <div class="col-md-4">
            <label for="">Show
                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                entries
            </label>
        </div>
        <div class="col-sm-3 text-center">
            <div>
                <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableComplete');">Export</button>
                <button class="btn btn-primary btn-sm" ng-click="printReport('tableComplete');">Print</button>
            </div>
        </div>
        <div class="col-md-5 text-right">
            <div class="form-inline form-group">
                <label for="filter-list">Search </label>
                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-horizontal">
            <div class="form-group">
                <label for="" class="col-sm-2 control-label">From</label>
                <div class="col-sm-3 input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                </div>
                <label for="" class="col-sm-1 control-label">To</label>
                <div class="col-sm-3 input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                </div>
                <button class="col-sm-1 btn btn-warning btn-sm" ng-click="searchCompletedDate(fromDate,toDate)">Search</button>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               <p class="text-primary">Assigned Orders and Details from {{fromDate | date:'dd-MMMM-yyyy'}} to {{toDate | date:'dd-MMMM-yyyy'}}</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tableComplete">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Order Date</th>
                            <th>Branch</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="pendingorder in listCount  = (pendingorders | filter:filterlist) | pagination: currentPage : numPerPage">
                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                            <td>{{pendingorder.order_date | date:'dd-MMMM-yyyy'}}</td>
                            <td><a ng-click="viewBranch(pendingorder);">{{pendingorder.branch.name}}</a></td>
                            <td>{{pendingorder.description}}</td>
                            <td>
                                <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                    <button type="button" class="btn btn-success btn-round" ng-click="assignOrder(pendingorder);">
                                        <!-- <i class="fa fa-pencil"></i>-->Update
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix" ng-show="pendingorders.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>
</div>