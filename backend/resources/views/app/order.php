<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Orders </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button  ng-if="user.permissions.order.write==='true'" ng-click="newOrder();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Order</button>
            <form class="form-horizontal" ng-show="orderedit" ng-submit="addOrder();">
                <h3>New Order</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Order Date</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="neworder.order_date" is-open="order_datepicker" show-button-bar="false" show-weeks="false" readonly>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" ng-click="order_datepicker=true"><i class="fa fa-calendar"></i></button>
								</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" ng-model="neworder.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div>
                    <div class="col-lg-13">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SlNo</th>
                                            <th>Row Meterial</th>
                                            <th>Qty</th>
                                            <th>Unit</th>
                                            <th>Priority</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="newitem in newitems">
                                                <td><div>{{$index+1}}</div></td>
                                                <td>
                                                    <div ng-hide="editingData[newitem.id]">{{newitem.name}}</div>
                                                    <div ng-show="editingData[newitem.id]"><select class="form-control" ng-model="newitem.row_meterials_id"> <option ng-repeat="row in rows" value="{{row.id}}" ng-selected="newitem.row_meterials_id == row.id">{{row.name}}</option></select></div>
                                                </td>
                                                <td>
                                                    <div ng-hide="editingData[newitem.id]">{{newitem.qty}}</div>
                                                    <div ng-show="editingData[newitem.id]"><input type="text" onkeypress="return isNumberKey(event)" class="form-control" ng-model="newitem.qty"></td></div>
                                                <td>
                                                    <div ng-hide="editingData[newitem.id]">{{newitem.unit}}</div>
                                                    <div ng-show="editingData[newitem.id]"><select class="form-control" ng-model="newitem.units_id"> <option ng-repeat="unit in units" value="{{unit.id}}">{{unit.name}}</option></select></div>
                                                </td>
                                                <td>
                                                    <div ng-hide="editingData[newitem.id]">{{newitem.priority}}</div>
                                                    <div ng-show="editingData[newitem.id]">
                                                        <select class="form-control" ng-model="newitem.priority">
                                                            <option  value="High">High</option>
                                                            <option  value="Medium">Medium</option>
                                                            <option  value="Low">Low</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td ng-show="newitems.length>0">
                                                    <div ng-hide="editingData[newitem.id]" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                        <button type="button" class="btn btn-success" ng-click="editItem(newitem);">Edit
                                                        </button>
                                                        <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                                        </button>
                                                        <button type="button" class="btn btn-default" ng-click="viewItem(newitem); editmode = !editmode">View
                                                        </button>
                                                    </div>
                                                    <div ng-show="editingData[newitem.id]"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                        <button type="button" class="btn btn-success" ng-click="updateItem(newitem);">Update
                                                        </button>
                                                        <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                                        </button>
                                                        <button type="button" class="btn btn-default" ng-click="viewItem(newitem); editmode = !editmode">View
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="addItem();">Add Item</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelOrder();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>

            <h3>Order and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableOrder');">Export</button>
                        <button class="btn btn-primary btn-sm" ng-click="printReport('tableOrder');">Print</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">From</label>
                    <div class="col-sm-3 input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <label for="" class="col-sm-1 control-label">To</label>
                    <div class="col-sm-3 input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <button class="col-sm-1 btn btn-warning btn-sm" ng-click="searchOrderDate(fromDate,toDate)">Search</button>
                </div>
            </div>
        </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="text-primary">Orders and Detailsfrom {{fromDate | date:'dd-MMMM-yyyy'}} to {{toDate | date:'dd-MMMM-yyyy'}}</p>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableOrder">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order Date</th>
                                    <th>Branch</th>
                                    <th>Description</th>
                                    <th>List</th>
                                    <th>Item</th>
                                    <th>Created</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in listCount  = (orders | filter:filterlist) | orderBy:'-order_date' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{order.order_date | date:'dd-MMMM-yyyy'}}</td>
                                    <td><a ng-click="viewBranch(order);">{{order.branch.name}}</a></td>
                                    <td><p class="description" popover="{{order.description}}" popover-trigger="mouseenter">{{order.description}}</p></td>
                                    <td>
                                        <ol>
                                            <li ng-repeat="item in order.order_item">
                                                {{item.row_meterial.name}}<span style="color: red;">({{item.qty}})</span>
                                            </li>
                                        </ol>
                                    </td>
                                    <td> <button type="button" class="btn btn-default" ng-click="viewOrderItem(order);">
                                           View Order Items
                                        </button></td>
                                    <td>
                                        {{order.created_at}}
                                    </td>
                                    <td>
                                        <div  ng-if="user.permissions.order.edit==='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-default" ng-click="editOrder(order);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" ng-click="deleteOrder(order); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="orders.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>

