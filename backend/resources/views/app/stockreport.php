

<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Stock Reports </h2>
    </div>
</div>
<div class="row">
<div class="col-md-12">
<div class="box">
<br>

<h3 ng-hide="rentedit"> Stock Reports </h3>
<div class="row">
    <div class="col-md-2">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>

    <div class="col-sm-6 text-center">
        <div>
            <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableRow');">Export</button>
            <button class="btn btn-primary btn-sm" ng-click="printReport('tableRow');">Print</button>
        </div>
    </div>
    <div class="col-md-4 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Row Materials Stock and Details
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tableRow">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>
                                        <span ng-click="sortType = 'name'; sortReverse = !sortReverse">
                                        Name
                                        <span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                        </th>
                        <th>
                                         <span ng-click="sortType = 'color'; sortReverse = !sortReverse">
                                        Colour
                                        <span ng-show="sortType == 'color' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'color' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                        </th>
                        <th>
                            <span ng-click="sortType = 'size'; sortReverse = !sortReverse">
                            Size
                            <span ng-show="sortType == 'size' && !sortReverse" class="fa fa-caret-down"></span>
                            <span ng-show="sortType == 'size' && sortReverse" class="fa fa-caret-up"></span>
                            </span>
                        </th>
                        <th>
                            <span ng-click="sortType = 'qty'; sortReverse = !sortReverse">
                            Qty
                            <span ng-show="sortType == 'qty' && !sortReverse" class="fa fa-caret-down"></span>
                            <span ng-show="sortType == 'qty' && sortReverse" class="fa fa-caret-up"></span>
                            </span>
                        </th>
                        <th>
                            <span ng-click="sortType = 'order'; sortReverse = !sortReverse">
                            Total Order
                            <span ng-show="sortType == 'order' && !sortReverse" class="fa fa-caret-down"></span>
                            <span ng-show="sortType == 'order' && sortReverse" class="fa fa-caret-up"></span>
                            </span>
                        </th>
                        <th>
                            <span ng-click="sortType = 'pending'; sortReverse = !sortReverse">
                            Pending
                            <span ng-show="sortType == 'pending' && !sortReverse" class="fa fa-caret-down"></span>
                            <span ng-show="sortType == 'pending' && sortReverse" class="fa fa-caret-up"></span>
                            </span>
                        </th>
                        <th>
                            Balance
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="stock in listCount  = (stocks | filter:filterlist) | orderBy:sortType:sortReverse | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td><a ng-click="open(stock);">{{stock.name}}</a></td>
                        <td>{{stock.color}}</td>
                        <td>{{stock.size}}</td>
                        <td>{{stock.qty}}</td>
                        <td>{{stock.order}}</td>
                        <td>{{stock.pending}}</td>
                        <td>{{(stock.qty)-(stock.order-stock.pending)}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-show="stocks.length > numPerPage">
    <pagination
        ng-model="currentPage"
        total-items="listCount.length"
        max-size="maxSize"
        items-per-page="numPerPage"
        boundary-links="true"
        class="pagination-sm pull-right"
        previous-text="&lsaquo;"
        next-text="&rsaquo;"
        first-text="&laquo;"
        last-text="&raquo;"
        ></pagination>
</div>
</div>
</div>
