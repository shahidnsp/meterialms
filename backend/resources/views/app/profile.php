<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Change Profile </h2>
    </div>
</div>
<div class="row">
    <form class="form-horizontal">
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" ng-model="curprofile.username" placeholder="New Username" required>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" ng-model="curprofile.name" placeholder="Name" required>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-9">
                <textarea class="form-control" ng-model="curprofile.address" placeholder="Address"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" ng-model="curprofile.phone" placeholder="Phone">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Mobile</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" ng-model="curprofile.mobile" placeholder="mobile">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-11 text-right">
                <button type="button" class="btn btn-default" ng-click="">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </form>
</div>
