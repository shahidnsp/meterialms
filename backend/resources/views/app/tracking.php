<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Material Order Tracking </h2>
    </div>
</div>
<div class="box">
    <h3>Material Order Tracking and details</h3>
    <div class="row">
        <div class="col-md-4">
            <label for="">Show
                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                entries
            </label>
        </div>
        <div class="col-sm-3 text-center">
            <div>
                <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableComplete');">Export</button>
                <button class="btn btn-primary btn-sm" ng-click="printReport('tableComplete');">Print</button>
            </div>
        </div>
        <div class="col-md-5 text-right">
            <div class="form-inline form-group">
                <label for="filter-list">Search </label>
                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Material</label>
                <div class="col-sm-4">
                    <select class="form-control" ng-model="row_meterials_id">
                        <option value="0">All Materials</option>
                        <option ng-repeat="row in rows" value="{{row.id}}">{{row.name}}</option>
                    </select>
                </div>
                <label class="col-sm-1 control-label">Branch</label>
                <div class="col-sm-4">
                    <select class="form-control" ng-model="branches_id">
                        <option value="0">All Branches</option>
                        <option ng-repeat="branch in branches" value="{{branch.id}}">{{branch.name}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-2 control-label">From</label>
                <div class="col-sm-3 input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                </div>
                <label for="" class="col-sm-2 control-label">To</label>
                <div class="col-sm-3 input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                </div>
                <button class="col-sm-1 btn btn-warning btn-sm" ng-click="search(fromDate,toDate,row_meterials_id,branches_id)">Search</button>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Material Orders and Details
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tableComplete">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Order Date</th>
                            <th>Branch</th>
                            <th>Item</th>
                            <th>Requested</th>
                            <th>Unit</th>
                            <th>Assigned</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="ord in listCount  = (orders | filter:filterlist) | pagination: currentPage : numPerPage">
                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                            <td>{{ord.order.order_date | date:'dd-MMMM-yyyy'}}</td>
                            <td>{{ord.branch}}</td>
                            <td><a ng-click="viewItem(ord);">{{ord.row_meterial.name}}</a></td>
                            <td>{{ord.qty}}</td>
                            <td>{{ord.unit.name}}</td>
                            <td>{{ord.assign}}</td>
                            <td>{{ord.balance}}</td>
                        </tr>
                        <tr class="btn-success">
                            <td></td><td></td><td></td><td>Total</td><td>{{totalQty}}</td><td></td><td>{{totalAssign}}</td><td>{{totalBalance}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix" ng-show="orders.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>
</div>