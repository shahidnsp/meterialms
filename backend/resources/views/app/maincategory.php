<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Main Category</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <br>
            <button ng-if="user.permissions.master.write==='true'"  ng-click="newMain();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Main Category</button>
            <form class="form-horizontal" ng-show="mainedit" ng-submit="addMain();">
                <h3>New Main</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newmain.name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newmain.description" placeholder="Name"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelMain();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Main Category and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableMain');">Export</button>
                        <button class="btn btn-primary btn-sm" ng-click="printReport('tableMain');">Print</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Mains and Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableMain">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="main in listCount  = (mains | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{main.name}}</td>
                                    <td><p class="description" popover="{{main.description}}" popover-trigger="mouseenter">{{main.description}}</p></td>
                                    <td>
                                        <div  ng-if="user.permissions.master.edit==='true'"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-default" ng-click="editMain(main);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button ng-hide="main.id=='1'" type="button" class="btn btn-default" ng-click="deleteMain(main); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="mains.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>