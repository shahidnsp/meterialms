<div class="row">
    <div class="col-lg-12">
        <h1>{{user.branch}} / Admin Dashboard </h1>
    </div>
</div>
<hr />
<!--BLOCK SECTION -->
<div class="row">
    <div ng-show="user.isAdmin=='A'" class="col-lg-12">
        <div style="text-align: center;">

            <a class="quick-btn" href="#rowmaterial">
                <i class="icon-th icon-2x"></i>
                <span> Row Material </span>
                <span class="label label-danger">{{materialcount}}</span>
            </a>

            <a class="quick-btn" href="#branch">
                <i class="icon-bold icon-2x"></i>
                <span>Branches</span>
                <span class="label label-success">{{branchcount}}</span>
            </a>
            <a class="quick-btn" href="#vendor">
                <i class="icon-list-alt icon-2x"></i>
                <span>Vendors</span>
               <!-- <span class="label label-warning">+25</span>-->
            </a>
            <a class="quick-btn" href="#order">
                <i class="icon-external-link icon-2x"></i>
                <span>New Orders</span>
            </a>
            <a class="quick-btn" href="#pendingorder">
                <i class="icon-list icon-2x"></i>
                <span>Order</span>
                <span class="label label-default">{{pndngOrder}}</span>
            </a>
            <a class="quick-btn" href="#purchase">
                <i class="icon-shopping-cart icon-2x"></i>
                <span>Purchase</span>
                <!--<span class="label label-default">20</span>-->
            </a>



        </div>

    </div>

</div>
<!--END BLOCK SECTION -->
<hr ng-show="users.isAdmin=='A'" />
<!-- CHART & CHAT  SECTION -->
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Real Time Traffic
            </div>

            <div class="demo-container">
                <div
                    data-ac-chart="'pie'"
                    data-ac-data="data"
                    data-ac-config="config"
                    style="height: 100%;width: 100%;">
                </div>
            </div>

        </div>

    </div>


    <div class="col-lg-4">

        <div class="chat-panel panel panel-primary">
            <div class="panel-heading">
                <i class="icon-comments"></i>
                Chat
                <!--<div class="btn-group pull-right">
                    <button type="button" data-toggle="dropdown">
                        <i class="icon-chevron-down"></i>
                    </button>
                    <ul class="dropdown-menu slidedown">
                        <li>
                            <a href="#">
                                <i class="icon-refresh"></i> Refresh
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class=" icon-comment"></i> Available
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-time"></i> Busy
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-tint"></i> Away
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <i class="icon-signout"></i> Sign Out
                            </a>
                        </li>
                    </ul>
                </div>-->
            </div>

            <div class="panel-body">
                <ul class="chat">
                    <li ng-repeat="message in messages" class="left clearfix">
                                        <span class="chat-img pull-left">
                                            <img src="img/2.png" alt="User Avatar" class="img-circle" />
                                        </span>

                        <div class="chat-body clearfix">
                            <div class="header">
                                <strong class="primary-font"> {{message.user.username}} </strong>
                                <small class="pull-right text-muted">
                                    <i class="icon-time"></i> {{message.date}}
                                </small>

                            </div>
                            <br />
                            <p>
                                {{message.message}}
                            </p>
                            <button type="button" class="btn btn-success btn-xs" ng-click="deleteMessage(message); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="panel-footer">
                <form ng-submit="addMessage()">
                <div class="input-group">
                    <input id="Text1" type="text" class="form-control input-sm" ng-model="newmessage.message" placeholder="Type your message here..." />
                                    <span class="input-group-btn">
                                        <button class="btn btn-warning btn-sm"  id="Button1">
                                            Send
                                        </button>
                                    </span>
                </div>
                </form>
            </div>

        </div>


    </div>
</div>
<!--END CHAT & CHAT SECTION -->
<!-- COMMENT AND NOTIFICATION  SECTION -->
<div class="row">
    <div class="col-lg-7">

        <div class="chat-panel panel panel-success">
            <div class="panel-heading">
                <i class="icon-tasks"></i>
                Task List

            </div>

            <div class="panel-body">
                <ul class="chat">
                    <li ng-repeat="task in tasks" class="left clearfix">
                                       <!-- <span class="chat-img pull-left">
                                            <img src="img/2.png" alt="User Avatar" class="img-circle" />
                                        </span>-->
                        <!--<div class="chat-body clearfix">-->
                            <div class="header">
                                <strong class="primary-font "> {{task.name}} </strong>
                                <small class="pull-right text-muted label label-danger">
                                    <i class="icon-time"></i> {{task.fromDate}}
                                </small>
                                <small class="pull-right text-muted label label-danger">
                                    <i class="icon-time"></i> {{task.toDate}}
                                </small>
                                <button type="button" class="btn btn-success btn-xs" ng-click="deleteTask(task); editmode = !editmode">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                            <br />
                            <p>
                                {{task.description}}
                            </p>
                       <!-- </div>-->
                    </li>
                </ul>
            </div>

           <!-- <div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your comment here..." />
                                    <span class="input-group-btn">
                                        <button class="btn btn-success btn-sm" id="btn-chat">
                                            Send
                                        </button>
                                    </span>
                </div>
            </div>-->

        </div>


    </div>
    <div class="col-lg-5">

        <div class="panel panel-danger">
            <div class="panel-heading">
                <i class="icon-bell"></i> New Task
            </div>

            <div class="panel-body">
                <!--<div class="list-group">

                </div>-->
                <div class="list-group">
                    <form ng-submit="addTask()">
                    <div class="form-group">
                        <label for="taskname" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" id="taskname" class="form-control" ng-model="newtask.name" placeholder="Name" required>
                        </div>
                    </div><br><br>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Note</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" ng-model="newtask.description" placeholder="Description"></textarea>
                        </div>
                    </div><br><br><br>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">From</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newtask.fromDate" is-open="debutpicker" show-button-bar="false" show-weeks="false" readonly>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" ng-click="debutpicker=true"><i class="fa fa-calendar"></i></button>
								</span>
                            </div>
                        </div>
                    </div><br><br>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newtask.toDate" is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
								</span>
                            </div>
                        </div>
                    </div><br><br>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    </form>
                </div>


            </div>

        </div>



    </div>
</div>

<!--<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Real Time Traffic
            </div>

            <div class="demo-container">
                <div
                    data-ac-chart="'bar'"
                    data-ac-data="data"
                    data-ac-config="config"
                    style="height: 100%;width: 100%;">
                </div>
            </div>
        </div>
    </div>
</div>-->

<!--TABLE, PANEL, ACCORDION AND MODAL  -->
<div class="row">
    <div class="col-lg-6">
        <div class="box">
            <header>
                <h5>Stock Reminder</h5>
                <div class="toolbar">
                    <div class="btn-group">
                        <a data-toggle="collapse" class="btn btn-default btn-sm accordion-toggle minimize-box">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>
            </header>
            <div id="sortableTable" class="body collapse in">
                <table class="table table-bordered sortableTable responsive-table">
                    <thead>
                    <tr>
                        <th>#<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
                        <th>Material<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
                        <th>Stock<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
                    </tr>
                    </thead>
                    <tbody>


                    <tr ng-repeat="reminder in listCount  = (reminders) | orderBy:'-name' | pagination: currentPage : numPerPage"">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td><a ng-click="open(reminder);">{{reminder.name}}</a></td>
                        <td>{{reminder.qty}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="clearfix">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>

    </div>


</div>
<!--TABLE, PANEL, ACCORDION AND MODAL  -->