

<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Row Materials </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <br>
            <button ng-if="user.permissions.rowmaterial.write==='true'"  ng-click="newRow();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Row Material</button>
            <form class="form-horizontal" ng-show="rowedit" ng-submit="addRow();" enctype="multipart/form-data">
                <h3>New Row Material</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newrow.name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newrow.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Colour</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newrow.color" placeholder="Colour">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Size</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newrow.size" placeholder="Size">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Image</label>
                    <div class="col-sm-9">
                        <input type="file" class="upload"  file-model="newrow.photo" placeholder="Image">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Quantity</label>
                    <div class="col-sm-9">
                        <input type="text" onkeypress="return isNumberKey(event)" class="form-control" ng-model="newrow.qty" placeholder="Quantity" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Warning Quantity</label>
                    <div class="col-sm-9">
                        <input type="text" onkeypress="return isNumberKey(event)" class="form-control" ng-model="newrow.warning_qty" placeholder="Warning Quantity">
                    </div>
                </div>
                <!--<div class="form-group">
                    <label for="" class="col-sm-2 control-label">Expected Date</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newrow.expeted_date" placeholder="Expected Date">
                    </div>
                </div>-->
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Unit</label>
                    <div class="col-sm-9" >
                        <select class="form-control" ng-model="newrow.units_id">
                            <option ng-repeat="unit in units" value="{{unit.id}}">{{unit.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Main category</label>
                    <div class="col-sm-9" >
                        <select class="form-control" ng-model="newrow.main_categories_id">
                            <option ng-repeat="maincategory in maincategories" value="{{maincategory.id}}">{{maincategory.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Sub Category</label>
                    <div class="col-sm-9" >
                        <select class="form-control" ng-model="newrow.sub_categories_id">
                            <option ng-repeat="subcategory in subcategories" value="{{subcategory.id}}">{{subcategory.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Vendor</label>
                    <div class="col-sm-9" >
                        <select class="form-control" ng-model="newrow.venders_id">
                            <option ng-repeat="vendor in vendors" value="{{vendor.id}}">{{vendor.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Location</label>
                    <div class="col-sm-9">
                        <select class="form-control" ng-model="newrow.storage_locations_id">
                            <option ng-repeat="storage in storages" value="{{storage.id}}">{{storage.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelRow();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Row Materials and details</h3>
            <div class="row">
                <div class="col-md-2">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-3">
                    <div class="form-inline form-group">
                        <label for="filter-list">Qty Below </label>
                        <input type="text" class="form-control" placeholder="Qty">
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableRow');">Export</button>
                        <button class="btn btn-primary btn-sm" ng-click="printReport('tableRow');">Print</button>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Row Materials and Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableRow">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>
                                        <span ng-click="sortType = 'name'; sortReverse = !sortReverse">
                                        Name
                                        <span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                                    </th>
                                    <th>
                                        <span ng-click="sortType = 'description'; sortReverse = !sortReverse">
                                        Description
                                        <span ng-show="sortType == 'description' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'description' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                                    </th>
                                    <th>
                                         <span ng-click="sortType = 'color'; sortReverse = !sortReverse">
                                        Colour
                                        <span ng-show="sortType == 'color' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'color' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                                    </th>
                                    <th>
                                        <span ng-click="sortType = 'size'; sortReverse = !sortReverse">
                                        Size
                                        <span ng-show="sortType == 'size' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'size' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                                    </th>
                                    <th>Photo</th>
                                    <th>
                                        <span ng-click="sortType = 'qty'; sortReverse = !sortReverse">
                                        Qty
                                        <span ng-show="sortType == 'qty' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'qty' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                                    </th>
                                    <th>
                                        <span ng-click="sortType = 'warning_qty'; sortReverse = !sortReverse">
                                        Warn.Qty
                                        <span ng-show="sortType == 'warning_qty' && !sortReverse" class="fa fa-caret-down"></span>
                                        <span ng-show="sortType == 'warning_qty' && sortReverse" class="fa fa-caret-up"></span>
                                        </span>
                                    </th>
                                    <!--<th>Unit</th>
                                    <th>Main Category</th>
                                    <th>Sub Category</th>
                                    <th>Vendor</th>
                                    <th>Storage Location</th>-->
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="row in listCount  = (rows | filter:filterlist) | orderBy:sortType:sortReverse | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td><a ng-click="open(row)">{{row.name}}</a></td>
                                    <td><p class="description" popover="{{row.description}}" popover-trigger="mouseenter">{{row.description}}</p></td>
                                    <td>{{row.color}}</td>
                                    <td>{{row.size}}</td>
                                    <td><img ng-click="viewPhoto(row);" id="image"  ng-src="{{row.photo}}" width="60px" height="60px"></td>
                                    <td>{{row.qty}}</td>
                                    <td>{{row.warning_qty}}</td>
                                    <!--<td>{{row.unit.name}}</td>
                                    <td>{{row.main_category.name}}</td>
                                    <td>{{row.sub_category.name}}</td>
                                    <td>{{row.vendor.name}}</td>
                                    <td>{{row.storage_location.name}}</td>-->
                                    <td>
                                        <div ng-if="user.permissions.rowmaterial.edit==='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-default" ng-click="editRow(row);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" ng-click="deleteRow(row); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="rows.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
