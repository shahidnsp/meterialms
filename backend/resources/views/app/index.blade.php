<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8" > <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9" > <![endif]-->
<!--[if !IE]><!--> <html lang="en" ng-app="myApp"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>MATERIAL Management System | Dashboard </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <link rel="stylesheet" href="[[ elixir("css/app.css")]]">
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " ng-controller="homecontroller">

    <!-- MAIN WRAPPER -->
    <div id="wrap" >


        <!-- HEADER SECTION -->
        <div id="top">

            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 10px;">
                <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!-- LOGO SECTION -->
                <header class="navbar-header">
                    <div>
                         <a href="#/" class="navbar-brand">
                                            <img src="img/logo.png" alt="" />

                                                </a>
                    </div>

                </header>
                    <div class="col-sm-6">
                       <h4 class="text-primary" style="text-align: center">{{user.branch}}</h4>
                    </div>
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-right">



                    <!--ADMIN SETTINGS SECTIONS -->

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#/settings"><i class="icon-user"></i> User Profile </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="[[URL::route('logout')]]"><i class="icon-signout"></i> Logout </a>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>

        </div>
        <!-- END HEADER SECTION -->
                <br>

        <!-- MENU SECTION -->
       <div id="left" >
            <div class="media user-media well-small">
                <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="img/user.gif" />
                </a>
                <br />
                <div class="media-body">
                    <h5 class="media-heading"> {{user.name}} </h5>
                    <ul class="list-unstyled user-info">
                        <li>
                             <a class="btn btn-success btn-xs btn-circle" style="width: 10px;height: 12px;"></a> Online
                        </li>
                    </ul>
                </div>
                <br />
            </div>
            <br>
            <ul id="menu" class="collapse" >





                 <li ng-repeat="menu in user.menu" ng-if="menu.name!='master'"  ng-class="{ active: isActive('/{{menu.name}}')}"><a  href="#{{menu.name}}"><i class="icon-{{menu.icon}}"></i> {{menu.title}} <span ng-if="menu.name=='pendingorder'" class="label label-danger">{{pndngOrder}}</span> </a></li>
                 <li ng-repeat="menu in user.menu" ng-if="menu.name=='master'" class="panel ">
                     <a data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                           <i class="icon-tasks"> </i> Master Settings

                           <span class="pull-right">
                             <i class="icon-angle-down"></i>
                           </span>
                          &nbsp; <span class="label label-default">4</span>&nbsp;
                          </a>
                          <ul class="collapse" id="component-nav">
                              <li class=""><a href="#unit"><i class="icon-angle-right"></i> Units </a></li>
                               <li class=""><a href="#maincategory"><i class="icon-angle-right"></i> Main Category </a></li>
                              <li class=""><a href="#subcategory"><i class="icon-angle-right"></i> Sub Category </a></li>
                              <li class=""><a href="#storagelocation"><i class="icon-angle-right"></i> Storage Location </a></li>
                          </ul>
                 </li>
            </ul>

        </div>
        <!--END MENU SECTION -->



        <!--PAGE CONTENT -->
        <div id="content">

            <div class="inner" style="min-height: 700px;">
                 <br/>
                 <div ng-view></div>
            </div>

        </div>
        <!--END PAGE CONTENT -->

         <!-- RIGHT STRIP  SECTION -->
        <div id="right">


            <div class="well well-small">
                <ul class="list-unstyled">
                    <li>Row Material &nbsp; : <span>{{materialcount}}</span></li>
                    <li>Users &nbsp; : <span>{{usercount}}</span></li>
                    <li>Pending Order &nbsp; : <span>{{pndngOrder}}</span></li>
                </ul>
            </div>
            <div class="well well-small">
                <button ng-click="go('/')" class="btn btn-block"> Home </button>
                <button ng-click="go('order')" class="btn btn-info btn-block"> Order</button>
                <button ng-click="go('settings')" class="btn btn-info btn-block"> Profile </button>
                <button ng-click="go('settings')" class="btn btn-info btn-block"> Change Password </button>
                <a href="[[URL::route('logout')]]" class="btn btn-warning btn-block">Logout  </a>
            </div>
            <div class="well well-small">
                <span>Material</span><span class="pull-right"><small>{{materialcount}}</small></span>

                <div class="progress mini">
                    <div class="progress-bar progress-bar-info" style="width: 20%"></div>
                </div>
                <span>Completed</span><span class="pull-right"><small>{{cmptOrder}}</small></span>

                <div class="progress mini">
                    <div class="progress-bar progress-bar-success" style="width: 40%"></div>
                </div>
                <span>Pending</span><span class="pull-right"><small>{{pndngOrder}}</small></span>

                <div class="progress mini">
                    <div class="progress-bar progress-bar-warning" style="width: 60%"></div>
                </div>
                <!--<span>Summary</span><span class="pull-right"><small>80%</small></span>

                <div class="progress mini">
                    <div class="progress-bar progress-bar-danger" style="width: 80%"></div>
                </div>-->
            </div>

        </div>
         <!-- END RIGHT STRIP  SECTION -->
    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p> powered By <a href="http://www.psybotechnologies.com">Psybo Technologies</a>  &nbsp;&copy; <?php echo date('Y') ?> &nbsp;</p>
    </div>
    <!--END FOOTER -->

     <script src="[[ elixir("js/angularjs.min.js") ]]"></script>
     <script src="[[ elixir("js/template.min.js") ]]"></script>
     <script src="[[ elixir("js/app.js") ]]"></script>

    <script language=Javascript>
    <!--
            function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
    //-->
    </script>
</body>

    <!-- END BODY -->
</html>
