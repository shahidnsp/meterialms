

<button type="button" class="close" ng-click="cancel();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <h3 class="modal-title">Ordered Items</h3>
</div>
<div class="modal-body1">

    <div class="container">
        <div class="row">

            <div class="toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Order Date:{{items[0].order.order_date | date:'dd-MMMM-yyyy'}}</h3>
                    </div>

                    <div class="col-lg-16">
                        <br/>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Orders and Details
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SlNo</th>
                                            <th>Material</th>
                                            <th>Qty</th>
                                            <th>Unit</th>
                                            <th>Assigned</th>
                                            <th>Balance</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in items">
                                            <td>{{$index+1}}</td>
                                            <td><a ng-click="viewItem(item);">{{item.row_meterial.name}}</a></td>
                                            <td>{{item.qty}}</td>
                                            <td>{{item.unit.name}}</td>
                                            <td>{{item.assign}}</td>
                                            <td>{{item.balance}}</td>
                                            <td>{{item.priority}}</td>
                                            <td>
                                                <p class="label label-warning" ng-show="item.status=='0'">Pending</p>
                                                <p class="label label-success" ng-show="item.status=='1'">Completed</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



