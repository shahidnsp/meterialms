<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Vendors </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <br>
            <button ng-if="user.permissions.vendor.write==='true'" ng-click="newVendor();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Vendor</button>
            <form class="form-horizontal" ng-show="vendoredit" ng-submit="addVendor();">
                <h3>New Vendor</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newvendor.name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newvendor.address" placeholder="Address" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Place</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newvendor.place" placeholder="Place">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newvendor.phone" placeholder="Phone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newvendor.mobile" placeholder="Mobile">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Contact</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newvendor.contact_person" placeholder="Contact Person">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelVendor();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Vendor Category and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableVendor');">Export</button>
                        <button class="btn btn-primary btn-sm" ng-click="printReport('tableVendor');">Print</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Vendors and Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableVendor">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Place</th>
                                    <th>Phone</th>
                                    <th>Mobile</th>
                                    <th>Contact Person</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="vendor in listCount  = (vendors | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{vendor.name}}</td>
                                    <td><p class="description" popover="{{vendor.address}}" popover-trigger="mouseenter">{{vendor.address}}</p></td>
                                    <td>{{vendor.place}}</td>
                                    <td>{{vendor.phone}}</td>
                                    <td>{{vendor.mobile}}</td>
                                    <td>{{vendor.contact_person}}</td>
                                    <td>
                                        <div ng-if="user.permissions.vendor.edit==='true'"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-default" ng-click="editVendor(vendor);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button ng-hide="vendor.id=='1'" type="button" class="btn btn-default" ng-click="deleteVendor(vendor); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="vendors.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>