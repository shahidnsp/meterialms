<!-- <div class="container-fluid" ng-controller="settingsController"> -->
<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-cog"></i> Basic changes</h2>
    </div>
</div>
<div class="row">
<tabset class="col-md-12">
<tab ng-if="user.isAdmin=='A'">
    <tab-heading><i class="fa fa-users"></i> Users</tab-heading>
    <div class="box">
        <div ng-hide="resetPermission">
            <button class="btn btn-primary pull-right" ng-click="newUser();" ng-hide="useredit"><i class="fa fa-plus"></i> Add User</button>
            <form class="form-horizontal" ng-show="useredit" ng-submit="addUser();">
                <h3>New User</h3>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">User Type</label>
                    <div class="col-sm-10">
                        <select class="form-control" ng-model="newuser.isAdmin">
                            <option value="u">User</option>
                            <option value="A">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newuser.name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" ng-model="newuser.address" placeholder="Address"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Branch</label>
                    <div class="col-sm-10">
                        <select class="form-control" ng-model="newuser.branches_id" required>
                            <option value="">Select Branch</option>
                            <option ng-repeat="branch in branches" value="{{branch.id}}">{{branch.name}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">User Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newuser.username" placeholder="User Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelUser();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3>User details</h3>
            <p>Here you can change all users login informations, Only administrator have permission for this</p>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-6 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Orders and Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Branch</th>
                                    <th>Mobile</th>
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="user in listCount = (users | filter:filterlist) | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{user.name}}</td>
                                    <td>{{user.branch.name}}</td>
                                    <td>{{user.mobile}}</td>
                                    <td>{{user.username}}</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-xs" ng-click="resetPassword(user)">
                                            <i class="fa fa-refresh"></i> reset
                                        </button>
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-danger" ng-click="permissionMode(user);">
                                                <i class="fa fa-key"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" ng-click="editUser(user);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" ng-click="deleteUser(user); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
        <div ng-show="resetPermission">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li ng-repeat="breadCrumb in breadCrumbs" ng-class="{active: $last}">
                            <a ng-if="!$last" href ng-click="backToUsers($event);"><i class="fa fa-home" ng-show="$first"></i> {{breadCrumb}}</a>
                            <span ng-if="$last">{{breadCrumb}}</span>
                        </li>
                    </ol>
                    <p>Select the pages you want add for this user from available pages and then you can add write,edit (White color is for not allowed) permission for those pages</p>
                </div>
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item active">
                            Permited pages
                            <a class="btn btn-info btn-xs pull-right" ng-click="savePermission()">Save</a>
                        </li>
                        <li class="list-group-item">
                            <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterMypermission" />
                        </li>
                        <li class="list-group-item" ng-repeat="mypermision in mypermisions | filter:filterMypermission">
                            {{mypermision.title}} <a href ng-click="removeToList(mypermision);"><i class="fa fa-arrow-right"></i></a>

                            <div class="btn-group btn-group-xs pull-right">
                                <button class="btn" ng-model="writePage" ng-click="toggleWrite(mypermision);" btn-checkbox ng-class="mypermision.write == 'true'? 'btn-primary' : 'btn-default'">
                                    write
                                </button>
                                <button class="btn btn-default" ng-model="editPage" ng-click="toggleEdit(mypermision);" btn-checkbox  ng-class="mypermision.edit == 'true'? 'btn-primary' : 'btn-default'">
                                    Edit
                                </button>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item active">Available pages</li>
                        <li class="list-group-item">
                            <input type="text" name="SearchDualList" class="form-control" placeholder="search" ng-model="filterPage" />
                        </li>
                        <li class="list-group-item" ng-repeat="page in pages | filter:filterPage">
                            <a href ng-click="addToList(page);"><i class="fa fa-arrow-left"></i></a> {{page.title}}
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <Button class="btn btn-success col-md-12" ng-click="savePermission()">Save Updates</Button>
                </div>
            </div>

        </div>
    </div>
</tab>
<tab>
    <tab-heading><i class="fa fa-user"></i> Profile</tab-heading>
    <div class="box">
        <p>You can change your contact personal details here</p>
        <table class="table table-hover table-responsive">
            <tbody>
            <tr>
                <th>Name</th>
                <td ng-if="!editName">{{userdetails.name}}</td>
                <td ng-if="editName"><input type="text" ng-model="inputs.name"></td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <button type="button" class="btn btn-default ng-hide" ng-hide="editName" ng-click="editDetail('name', userdetails.name); editName = !editName">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editName" ng-click="saveDetail('name',inputs.name); editName = !editName">
                            <i class="fa fa-save"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editName" ng-click="editName = !editName">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </td>
            </tr>

            <tr>
                <th>Address</th>
                <td ng-if="!editaddress">{{userdetails.address}}</td>
                <td ng-if="editaddress"><input type="text" ng-model="inputs.address"></td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <button type="button" class="btn btn-default ng-hide" ng-hide="editaddress" ng-click="editDetail('address', userdetails.address); editaddress = !editaddress">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editaddress" ng-click="saveDetail('address',inputs.address); editaddress = !editaddress">
                            <i class="fa fa-save"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editaddress" ng-click="editaddress = !editaddress">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </td>
            </tr>


            <tr>
                <th>Phone</th>
                <td ng-if="!editphone">{{userdetails.phone}}</td>
                <td ng-if="editphone"><input type="text" ng-model="inputs.phone"></td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <button type="button" class="btn btn-default ng-hide" ng-hide="editphone" ng-click="editDetail('phone', userdetails.phone); editphone = !editphone">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editphone" ng-click="saveDetail('phone',inputs.phone); editphone = !editphone">
                            <i class="fa fa-save"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editphone" ng-click="editphone = !editphone">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </td>
            </tr>
            <tr>
                <th>Mobile</th>
                <td ng-if="!editmobile1">{{userdetails.mobile}}</td>
                <td ng-if="editmobile1"><input type="text" ng-model="inputs.mobile1"></td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <button type="button" class="btn btn-default ng-hide" ng-hide="editmobile1" ng-click="editDetail('mobile1', userdetails.mobile1); editmobile1 = !editmobile1">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editmobile1" ng-click="saveDetail('mobile1',inputs.mobile1); editmobile1 = !editmobile1">
                            <i class="fa fa-save"></i>
                        </button>
                        <button type="button" class="btn btn-default" ng-show="editmobile1" ng-click="editmobile1 = !editmobile1">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</tab>
<tab>
    <tab-heading><i class="fa fa-key"></i> Password</a></tab-heading>
    <div class="box">
        <h4>Change your password</h4>
        <form class="form-horizontal" role="form" ng-submit="changePassword()">
            <div class="form-group">
                <label for="current-password" class="col-sm-2 control-label">Current/New Username</label>
                <div class="col-sm-6">
                    <input autocomplete="disabled"  ng-model="password.username" id="current-username" type="text" class="form-control" placeholder="Current/New Username">
                </div>
            </div>
            <div class="form-group">
                <label for="current-password" class="col-sm-2 control-label">Current Password</label>
                <div class="col-sm-6">
                    <input  autocomplete="disabled" ng-model="password.oldPassword" id="current-password" type="password" class="form-control" placeholder="Current Password">
                </div>
            </div>
            <div class="form-group">
                <label for="new-password" class="col-sm-2 control-label">New Password</label>
                <div class="col-sm-6">
                    <input  ng-model="password.newpassword" id="new-password" type="password" class="form-control" placeholder="New Password">
                </div>
            </div>
            <div class="form-group">
                <label for="repeat-password" class="col-sm-2 control-label">Repeat Password</label>
                <div class="col-sm-6">
                    <input  ng-model="password.password" id="repeat-password" type="password" class="form-control" placeholder="Repeat Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save change</button>
                </div>
            </div>
        </form>
    </div>
</tab>
</tabset>
</div>
