<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Branches </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div cgitlass="box">

            <button ng-if="user.permissions.branch.write==='true'"  ng-click="newBranch();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Branch</button>
            <form class="form-horizontal" ng-show="branchedit" ng-submit="addBranch();">
                <h3>New Branch</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbranch.name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newbranch.address" placeholder="Address" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Place</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbranch.place" placeholder="Place" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbranch.phone" placeholder="Phone">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbranch.mobile" placeholder="Mobile">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Supervisor</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbranch.supervisor" placeholder="Supervisor" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelBranch();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Branch Category and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableBranch');">Export</button>
                        <button class="btn btn-primary btn-sm" ng-click="printReport('tableBranch');">Print</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Branches and Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableBranch">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Place</th>
                                    <th>Phone</th>
                                    <th>Mobile</th>
                                    <th>Supervisor</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="branch in listCount  = (branchs | filter:filterlist) | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{branch.name}}</td>
                                    <td><p class="description" popover="{{branch.address}}" popover-trigger="mouseenter">{{branch.address}}</p></td>
                                    <td>{{branch.place}}</td>
                                    <td><p class="description" popover="{{branch.phone}}" popover-trigger="mouseenter">{{branch.phone}}</p></td>
                                    <td><p class="description" popover="{{branch.mobile}}" popover-trigger="mouseenter">{{branch.mobile}}</p></td>
                                    <td>{{branch.supervisor}}</td>
                                    <td>
                                        <div ng-if="user.permissions.branch.edit=='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-default" ng-click="editBranch(branch);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button ng-hide="branch.id=='1'" type="button" class="btn btn-default" ng-click="deleteBranch(branch); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="branchs.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>